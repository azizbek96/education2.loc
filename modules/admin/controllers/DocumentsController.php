<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Courses;
use app\modules\admin\models\DocumentItems;
use app\modules\admin\models\Documents;
use app\modules\admin\models\DocumentsSearch;
use app\modules\admin\models\StudentWaiting;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DocumentsController implements the CRUD actions for Documents model.
 */
class DocumentsController extends Controller
{
    public $slug;
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }
    public function beforeAction($action)
    {
        $this->slug = \Yii::$app->request->get('slug');
        if(parent::beforeAction($action))
        {
            $slug=Yii::$app->request->get('slug');

            $flag=false;
            if(!empty($slug))
            {
                if(array_key_exists($slug,Documents::getDocTypeBySlug()))
                {
                    $flag=true;
                    $this->slug=$slug;
                }
            }
            if(!($flag))
            {
                throw new NotFoundHttpException('manzil topilmadi');
            }
            return true;
            throw new ForbiddenHttpException(Yii::t('app','Access Denied'));
        }
        else{
            return false;
        }
    }
    /**
     * Lists all Documents models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new DocumentsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Documents model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        $searchModel = new DocumentsSearch();
        $models = $searchModel->search_($id);
        return $this->render('view', [
            'model' =>$model,
            'models' => $models,
        ]);
    }

    /**
     * Creates a new Documents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Documents();
        $models = [new DocumentItems()];
        switch ($this->slug){
            case   Documents::DOC_TYPE_INCOMING_LABEL:
                $model->document_type=Documents::DOC_TYPE_INCOMING;
                $number=Documents::getLastDocType($model->document_type);
                $model->document_number="DI-".$number."/".date('Ymd');
                break;
            case   Documents::DOC_TYPE_OUTGOING_LABEL:
                $model->document_type=Documents::DOC_TYPE_OUTGOING;
                $number=Documents::getLastDocType($model->document_type);
                $model->document_number='DO-'.$number.'/'.date('Ymd');
                break;
        }
        if ($this->request->isPost) {
            $flag=false;
            $data=Yii::$app->request->post();
            $transaction=Yii::$app->db->beginTransaction();
            try {
                switch ($model->document_type){
                    case Documents::DOC_TYPE_INCOMING:
                        if(!empty($data) && $model->load($data))
                        {
                            if($model->save() && !empty($data['DocumentItems']))
                            {
                                foreach ($data['DocumentItems'] as $item):
                                    $item_model=new DocumentItems([
                                        'doc_id'=>$model->id,
                                        'student_id'=>$item['student_id'],
                                        'price'=>$item['price'],
                                        'price_date'=>$item['price_date'],
                                        'type'=>Documents::DOC_TYPE_INCOMING,
                                        'add_info'=>$item['add_info']
                                    ]);
                                    if($item_model->save())
                                    {
                                        $flag=true;
                                    }
                                    else{
                                        $flag=false;
                                        break 2;
                                    }
                                endforeach;

                            }
                        }
                    break;
                    case Documents::DOC_TYPE_OUTGOING:
                        if(!empty($data) && $model->load($data))
                        {
                            if($model->save() && !empty($data['DocumentItems']))
                            {
                                foreach ($data['DocumentItems'] as $item):
                                    $item_model=new DocumentItems([
                                        'doc_id'=>$model->id,
                                        'student_id'=>$item['student_id'],
                                        'price'=>(-1)*$item['price'],
                                        'price_date'=>$item['price_date'],
                                        'type'=>Documents::DOC_TYPE_INCOMING,
                                        'add_info'=>$item['add_info']
                                    ]);
                                    if($item_model->save())
                                    {
                                        $flag=true;
                                    }
                                    else{
                                        $flag=false;
                                        break 2;
                                    }
                                endforeach;

                            }
                        }
                    break;
                }
            }
            catch (\Exception $exception)
            {
                \yii\helpers\VarDumper::dump($exception->getMessage(),10,true);
                die();
            }
            if($flag)
            {
                $transaction->commit();
                Yii::$app->session->setFlash('success',Yii::t('app','Document saqlandi.'));
                return $this->redirect(['view', 'id' => $model->id,'slug'=>$this->slug]);
            }else{
                $transaction->rollBack();
                Yii::$app->session->setFlash('danger',Yii::t('app','Document saqlab bolmadi.'));
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'models'=>$models
        ]);
    }

    /**
     * Updates an existing Documents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $models = ($model->documentItems)?$model->documentItems:[new DocumentItems()];
        if ($this->request->isPost) {
            $flag=false;
            $data=Yii::$app->request->post();
            $transaction=Yii::$app->db->beginTransaction();
            try {
                switch ($model->document_type){
                    case Documents::DOC_TYPE_INCOMING:
                        if(!empty($data) && $model->load($data))
                        {
                            if($model->save() && !empty($data['DocumentItems']))
                            {
                                DocumentItems::deleteAll(['doc_id'=>$model->id]);
                                foreach ($data['DocumentItems'] as $item):
                                    $item_model=new DocumentItems([
                                        'doc_id'=>$model->id,
                                        'student_id'=>$item['student_id'],
                                        'price'=>$item['price'],
                                        'price_date'=>$item['price_date'],
                                        'type'=>Documents::DOC_TYPE_INCOMING,
                                        'add_info'=>$item['add_info']
                                    ]);
                                    if($item_model->save())
                                    {
                                        $flag=true;
                                    }
                                    else{
                                        $flag=false;
                                        break 2;
                                    }
                                endforeach;

                            }
                        }
                        break;
                    case Documents::DOC_TYPE_OUTGOING:
                        if(!empty($data) && $model->load($data))
                        {
                            if($model->save() && !empty($data['DocumentItems']))
                            {
                                foreach ($data['DocumentItems'] as $item):
                                    $item_model=new DocumentItems([
                                        'doc_id'=>$model->id,
                                        'student_id'=>$item['student_id'],
                                        'price'=>(-1)*$item['price'],
                                        'price_date'=>$item['price_date'],
                                        'type'=>Documents::DOC_TYPE_INCOMING,
                                        'add_info'=>$item['add_info']
                                    ]);
                                    if($item_model->save())
                                    {
                                        $flag=true;
                                    }
                                    else{
                                        $flag=false;
                                        break 2;
                                    }
                                endforeach;

                            }
                        }
                        break;
                }
            }
            catch (\Exception $exception)
            {
                \yii\helpers\VarDumper::dump($exception->getMessage(),10,true);
                die();
            }
            if($flag)
            {
                $transaction->commit();
                Yii::$app->session->setFlash('success',Yii::t('app','Document saqlandi.'));
                return $this->redirect(['view', 'id' => $model->id,'slug'=>$this->slug]);
            }else{
                $transaction->rollBack();
                Yii::$app->session->setFlash('danger',Yii::t('app','Document saqlab bolmadi.'));
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('update', [
            'model' => $model,
            'models'=>$models
        ]);
    }

    /**
     * Deletes an existing Documents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index','slug'=>$this->slug]);
    }

    /**
     * Finds the Documents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Documents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Documents::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionEmployee()
    {
        Yii::$app->response->format=Response::FORMAT_JSON;
        $employee_id=Yii::$app->request->post('id');
        $response['status']=false;
        if($employee_id)
        {
            $response['items']=Courses::getCoursesNameByEmployeeId($employee_id);
            $response['status']=true;
        }
        return $response;
    }

    public function actionCourse()
    {
        Yii::$app->response->format=Response::FORMAT_JSON;
        $course_id=Yii::$app->request->post('id');
        $response['status']=false;
        if($course_id)
        {
            $response['items']= StudentWaiting::getStudentByCourse($course_id);
            $response['status']=true;
        }
        return $response;
    }
}
