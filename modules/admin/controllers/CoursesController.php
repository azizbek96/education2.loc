<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\CourseDate;
use app\modules\admin\models\Courses;
use app\modules\admin\models\CoursesSearch;
use app\modules\admin\models\Subject;
use app\modules\admin\models\SubjectLevel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CoursesController implements the CRUD actions for Courses model.
 */
class CoursesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Courses models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CoursesSearch();
        $query=Courses::find();

        $dataProvider=new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Courses model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Courses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Courses();
        $data=$this->request->post();
        $flag=false;
        $transaction=Yii::$app->db->beginTransaction();

        try {
            if ($this->request->isPost) {
                if ($model->load($data) && $model->save()) {
                    if(!empty($data['Courses']['course_date']))
                    {
                        foreach ($data['Courses']['course_date'] as $item) {
                            $course_date= new CourseDate();
                            $course_date->setAttributes([
                                'name'=>$item['type'],
                                'course_id' => $model->id,
                                'type' =>$item['name'],
                                'course_time'=>$item['course_time'],
                            ]);
                            if($course_date->save())
                            { $flag=true; }
                            else{
                                $flag=false;
                                break ;
                            }
                        }
                    }
                }
            } else {
                $model->loadDefaultValues();
            }
            if($flag)
            {
                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            }else
            {
                $transaction->rollBack();
            }
        }catch (Exception $exception)
        {
            yii\helpers\VarDumper::dump($exception,10,true);
            die();
        }
        if (\Yii::$app->request->isAjax){
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Courses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('update',[
                'model' => $model,
            ]);
        }
        $data=$this->request->post();
        $model->course_date=CourseDate::find()->where(['course_id'=>$id])->all();
        $flag=false;
        $transaction=Yii::$app->db->beginTransaction();
        try {
            if ($this->request->isPost && $model->load($data)){
                if($model->save() && !empty($data['Courses']['course_date'])){
                    CourseDate::deleteAll(['course_id'=>$model->id]);
                    foreach ($data['Courses']['course_date'] as $item) {
                        $course_date= new CourseDate();
                        $course_date->setAttributes([
                            'name'=>Courses::getWeeks($item['type']),
                            'course_id' => $model->id,
                            'type' =>$item['type'],
                            'course_time'=>$item['course_time'],
                        ]);
                        if($course_date->save())
                        { $flag=true; }
                        else{
                            $flag=false;
                            break ;
                        }
                    }
                }
            }
            if($flag)
            {
                $transaction->commit();
                return $this->redirect(['index', 'id' => $model->id]);
            }
            else{
                $transaction->rollBack();
            }
        } catch (\Exception $exception)
        {
            yii\helpers\VarDumper::dump($exception,10,true);
            die();

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Courses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Courses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Courses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Courses::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionSubject() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $id = $parents[0];
                $out = SubjectLevel::getSubjectLevelId($id);
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
//                $out=[
//                    ['id'=>'Wore', 'name'=>'<sub-cat-name1>'],
//                    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
//                 ];
//
                return ['output'=>$out, 'selected'=>''];
            }
        }
        return ['output'=>'', 'selected'=>''];
    }
}
