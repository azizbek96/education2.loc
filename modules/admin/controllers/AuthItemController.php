<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\AuthItem;
use app\modules\admin\models\AuthItemChild;
use app\modules\admin\models\AuthItemSearch;
use Yii;
use yii\base\BaseObject;
use yii\db\Exception;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AuthItemController implements the CRUD actions for AuthItem model.
 */
class AuthItemController extends BaseController
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /***
     *
     */
    public function actionRule()
    {
        $searchModel = new AuthItemSearch();
        $searchModel->is_type = AuthItem::AUTH_ITEM_RULE;
        $dataProvider = $searchModel->search($this->request->queryParams,AuthItem::AUTH_ITEM_RULE);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /***
     *
     */
    public function actionPermission()
    {
        $searchModel = new AuthItemSearch();
        $searchModel->is_type = AuthItem::AUTH_ITEM_PERMISSION;
        $dataProvider = $searchModel->search($this->request->queryParams,AuthItem::AUTH_ITEM_PERMISSION);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Lists all AuthItem models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     *
     */


    /**
     * Displays a single AuthItem model.
     * @param string $name Name
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($name, $type)
    {
        return $this->render('view', [
            'model' => $this->findModel($name, $type),
            'type' => $type,
        ]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new AuthItem();
        $model->type = Yii::$app->request->get('type');
        if ($model->type == AuthItem::AUTH_ITEM_PERMISSION){
            $model->new_permissions = [
                ['name' => 'index', 'description' => 'Index'],
                ['name' => 'update', 'description' => 'Update'],
                ['name' => 'view', 'description' => 'View'],
                ['name' => 'delete', 'description' => 'Delete'],
                ['name' => 'create', 'description' => 'Create'],
            ];
        }

        if ($this->request->isPost) {
            if ($model->type == AuthItem::AUTH_ITEM_RULE){
                if ($model->load($this->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'name' => $model->name, 'type' => $model->type]);
                }
            }
//            VarDumper::dump($_POST,10,true);die();

            if ($model->type == AuthItem::AUTH_ITEM_PERMISSION && $model->load($this->request->post())){

                $transaction = Yii::$app->db->beginTransaction();
                $flag = false;
                $data = Yii::$app->request->post();
                try {
                    if (!empty($data['AuthItem']['new_permissions'])){
                        foreach ($data['AuthItem']['new_permissions'] as $new_permission){
                            if (AuthItem::findOne(['name' => $model->controller.'/'.$new_permission['name']]) == null){
                                $permission = new AuthItem([
                                    'name' => $model->controller.'/'.$new_permission['name'],
                                    'description' => $new_permission['description'],
                                    'type' => AuthItem::AUTH_ITEM_PERMISSION
                                ]);

                                if ($permission->save() == false){
                                    $flag=false;
                                    break;
                                }
                            }
                            $rule_rel = new AuthItemChild([
                                'parent' => $model->category,
                                'child' => $model->controller.'/'.$new_permission['name'],
                            ]);
                            if ($rule_rel->save()){
                                $flag = true;
                            }else{
                                $flag = false;
                                break;
                            }
                        }
                        if ($flag){
                            $transaction->commit();
                            return $this->redirect(['permission']);
                        }else{
                            $transaction->rollBack();
                            VarDumper::dump($rule_rel->getErrors(), 10, true);die();
                            VarDumper::dump($permission->getErrorrs(), 10, true);die();
                        }
                    }
                }catch (Exception $exception){
                    VarDumper::dump($exception, 10, true);die();
                }
                return $this->redirect('permission');
            }

        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $name Name
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($name, $type)
    {
        $model = $this->findModel($name, $type);

        $model->category = \Yii::$app->request->get('name')??null;

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'name' => $model->name, 'type'=>$type]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $name Name
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($name, $type)
    {
        $this->findModel($name, $type)->delete();

        if (AuthItem::AUTH_ITEM_PERMISSION == $type){
            return $this->redirect(['permission']);
        }
        return $this->redirect(['rule']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $name Name
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($name, $type)
    {
        if (($model = AuthItem::findOne(['name' => $name, 'type' => $type])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
