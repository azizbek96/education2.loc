<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Employees;
use app\modules\admin\models\EmployeesSearch;
use app\modules\admin\models\Position;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use zxbodya\yii2\imageAttachment\ImageAttachmentAction;

/**
 * EmployeesController implements the CRUD actions for Employees model.
 */
class EmployeesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Employees models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new EmployeesSearch();

        $query=Employees::find()
            ->alias('e')
            ->leftJoin(['p'=>'position'],'p.id=e.position_id')
            ->andWhere(['e.status'=>1]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Employees model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Employees model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Employees();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->imageFile=UploadedFile::getInstance($model,'imageFile');
                $model->exeFile=UploadedFile::getInstance($model,'exeFile');
                if(!empty($model->pos_name))
                {
                    $data=new Position();
                    $data->name=$model->pos_name;
                    $data->description=$model->pos_description;
                    $data->save();
                    $data->refresh();
                    $model->position_id=$data->id;
                }
                if(!empty($model->imageFile) && !empty($model->exeFile))
                {
                    if($model->uploadFile() && $model->uploadImg())
                    {
                        if($model->save(false))
                        {
                            return $this->redirect(['index']);
                        }
                        else{
                            echo "<pre>";
                                print_r($model->getErrors());
                            echo "</pre>";
                            die();
                        }
                    }

                }
                if($model->save())
                {
                    return  $this->redirect(['index']);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Employees model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImg=$model->photo;
        $oldFile=$model->file;
        $posId=$model->position_id;
        if ($this->request->isPost && $model->load($this->request->post())) {
            $model->imageFile=UploadedFile::getInstance($model,'imageFile');
            $model->exeFile=UploadedFile::getInstance($model,'exeFile');
            if(!empty($model->pos_name))
            {
                $data=$this->findModel($posId);
                $data->name=$model->pos_name;
                $data->description=$model->pos_description;
                $data->save();
                $data->refresh();
                $model->position_id=$data->id;
            }
            if(!empty($model->imageFile) && !empty($model->exeFile))
            {

                if($model->uploadFile() && $model->uploadImg())
                {
                    unlink('uploads/image/'.$oldImg);
                    unlink('uploads/file/'.$oldFile);
                    if($model->save(false))
                    {
                        return $this->redirect(['index']);
                    }
                    else{
                        echo "<pre>";
                        print_r($model->getErrors());
                        echo "</pre>";
                        die();
                    }
                }

            }
            if($model->save())
            {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Employees model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
       $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Employees model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Employees the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employees::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
