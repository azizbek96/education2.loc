<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\EmployeeRelSubject;
use app\modules\admin\models\EmployeeRelSubjectSearch;
use app\modules\admin\models\Position;
use app\modules\admin\models\Subject;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmployeesRelController implements the CRUD actions for EmployeeRelSubject model.
 */
class EmployeesRelController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all EmployeeRelSubject models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new EmployeeRelSubjectSearch();
        $query=EmployeeRelSubject::find();
//            ->alias('er')
//            ->leftJoin(['s'=>'subject'],'s.id=er.subject_id')
//            ->leftJoin(['e'=>'employees'],'e.id=er.employee_id')
//            ->where(['s.status'=>1])
//            ->andWhere(['e.status'=>1]);
        $dataProvider=new ActiveDataProvider([
            'query'=>$query
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmployeeRelSubject model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EmployeeRelSubject model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new EmployeeRelSubject();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                if(!empty($model->sub_name))
                {
                    $data=new Subject();
                    $data->name=$model->sub_name;
                    $data->level=$model->sub_level;
                    $data->save();
                    $model->subject_id=$data->id;
                }
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
                else
                {
                    echo "<pre>";
                        print_r($model->getErrors());
                    echo "</pre>";
                    die();
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EmployeeRelSubject model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EmployeeRelSubject model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EmployeeRelSubject model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return EmployeeRelSubject the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmployeeRelSubject::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
