<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\SubUsers;
use app\modules\admin\models\Users;
use yii\data\ActiveDataProvider;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SubUsersController implements the CRUD actions for SubUsers model.
 */
class SubUsersController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all SubUsers models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $id=\Yii::$app->user->id;;
        $model=Users::find()->where(['id'=>$id])->one();
        if(($user=SubUsers::find()->where(['user_id'=>$id])->one()) == null)
        {
            $data=new SubUsers();
            $data->user_id=$id;
            $data->username=$model->username;
            $data->password = $model->password;
            $data->save();

        }
        $user=SubUsers::find()->where(['user_id'=>$id])->one();
        return $this->render('index', [
            'model' => $user,
        ]);
    }

    /**
     * Displays a single SubUsers model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SubUsers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new SubUsers();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SubUsers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage=$model->image;
        if ($this->request->isPost && $model->load($this->request->post())) {
            $model->image=UploadedFile::getInstance($model,'image');
            if(!is_dir('uploads/user'))
            {
                FileHelper::createDirectory('uploads/user');
            }
            if($model->image->saveAs('uploads/user/'.$model->image->baseName.'.'.$model->image->extension))
            {
                if($oldImage)
                {
                    unlink('uploads/user/'.$oldImage);
                }
            }
            $model->image=$model->image->baseName.'.'.$model->image->extension;
            if(($users=Users::findOne(['id'=>$model->user_id])) !=null)
            {
                $users->username=$model->username;
                $users->save();
            }
            if($model->save(false))
            {
                return $this->redirect(['index']);
            }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    public function actionChangePassword($id)
    {
        $model = $this->findModel($id);
        if($model->load($this->request->post()))
        {
            if(($users=Users::findOne(['id'=>$model->user_id])) !=null)
            {
                $users->password=$model->password;
                $users->save();
            }
            if($model->save())
            {
                return  $this->redirect(['index']);
            }
        }
        return $this->render('password',[
            'model' =>$model
        ]);
    }
    /**
     * Deletes an existing SubUsers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SubUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return SubUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SubUsers::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
