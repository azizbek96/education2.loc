<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Students;
use app\modules\admin\models\StudentsSearch;
use app\modules\admin\models\StudentWaiting;
use app\modules\admin\models\SubjectLevel;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\Breadcrumbs;

/**
 * StudentsController implements the CRUD actions for Students model.
 */
class StudentsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Students models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new StudentsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Students model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Students model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Students();
        $data=Yii::$app->request->post();
        $subject_level_id=SubjectLevel::getSubjectName();
        $flag=false;
            if ($this->request->isPost) {
                if ($model->load($data)) {
                    
                    if($model->save() && !empty($data['Students']['subject_level_id']))
                    {
                       $student_wait=new StudentWaiting();
                       $student_wait->setAttributes([
                           'student_id'=>$model->id,
                           'subject_level_id'=>$data['Students']['subject_level_id'],
                           'wait_date'=>$data['Students']['wait_date'],
                       ]);
                       if($student_wait->save())
                       {
                           $flag=true;
                       }
                    }
                    if($flag)
                    {
                        return $this->redirect(['index', 'id' => $model->id]);
                    }else{
                        $model->save();
                        return $this->redirect(['index', 'id' => $model->id]);
                    }
                }
            } else {
                $model->loadDefaultValues();
            }

        if (Yii::$app->request->isAjax){
            return $this->renderAjax('create',[
                'model' => $model
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    /**
     *
     */
    public function actionCreateCourseRel()
    {
        $model = new Students();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {

                return $this->redirect(['course-rel-student/create', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('create',[
                'model' => $model
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Students model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $data=$this->request->post();
        $data=StudentWaiting::findOne(['student_id'=>$model->id]);
        $model->subject_level_id = $data['subject_level_id'];
        $model->wait_date=$data['wait_date'];

        if ($this->request->isPost && $model->load($this->request->post())) {
                if($model->save() && !empty($data['Students']['subject_level_id']))
                {
                    StudentWaiting::deleteAll(['student_id' => $model->id]);
                    $student_wait=new StudentWaiting();
                    $student_wait->setAttributes([
                        'student_id'=>$model->id,
                        'subject_level_id'=>$data['Students']['subject_level_id'],
                        'wait_date'=>$data['Students']['wait_date'],
                    ]);
                    if($student_wait->save())
                    {
                        $flag=true;
                    }
                }
                if($flag)
                {
                    return $this->redirect(['index', 'id' => $model->id]);
                }else{
                    $model->save();
                    return $this->redirect(['index', 'id' => $model->id]);
                }
            
        }
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('update',[
                'model' => $model
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Students model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Students model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Students the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Students::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
