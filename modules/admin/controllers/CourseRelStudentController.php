<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\CourseRelStudent;
use app\modules\admin\models\CourseRelStudentSearch;
use app\modules\admin\models\Courses;
use app\modules\admin\models\StudentWaiting;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CourseRelStudentController implements the CRUD actions for CourseRelStudent model.
 */
class CourseRelStudentController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all CourseRelStudent models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CourseRelStudentSearch();
        $query=CourseRelStudent::find();
//            ->alias('crs')
//            ->leftJoin(['c'=>'courses'],'c.id=crs.course_id')
//            ->leftJoin(['s'=>'students'],'s.id=crs.student_id')
//            ->where(['c.status'=>1])
//            ->where(['s.status'=>1]);
        $dataProvider = new ActiveDataProvider([
            'query' =>$query
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionIndexAll()
    {
        $model=CourseRelStudent::getStudentWaitingSubjectList();
        return $this->render('index-all',[
            'model' => $model
        ]);
    }

    /**
     * Displays a single CourseRelStudent model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionViewAll($course_id)
    {
        $model=CourseRelStudent::find()->where(['course_id'=>$course_id])->all();
        return $this->render('view-all', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new CourseRelStudent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new CourseRelStudent();

        $model->student_id = Yii::$app->request->get('id')??null;

        if(Yii::$app->request->isAjax)
        {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                if($model->save()) {
                    return $this->redirect(['index']);
                }else
                {
                    \yii\helpers\VarDumper::dump($model->getErrors(),10,true);
                    die();
                    
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function actionCreateAll()
    {
        $model=new CourseRelStudent();
        $level_id=Yii::$app->request->get('subject_level_id');
        $student_waiting=StudentWaiting::getStudentWaitingAll($level_id,null);
        $model->subject_level_id=$level_id;
        if ($this->request->isPost) {
            $data=Yii::$app->request->post();
            $transaction=Yii::$app->db->beginTransaction();
            $flag=false;
            $CourseRelStudent=Yii::$app->request->post('CourseRelStudent');
            $course_id=$CourseRelStudent['course_id'];
            if(!empty($data) && !empty($data['CourseRelStudentAll']))
            {
                foreach ($data['CourseRelStudentAll'] as $value){
                    if(isset($value['check']) && !empty($value['check'])){
                        $new_model=new CourseRelStudent();
                        $new_model->setAttributes([
                            'course_id' =>$course_id,
                            'student_id'=>$value['student'],
                            'start_date'=>$CourseRelStudent['start_date'],
                            'end_date' => $CourseRelStudent['end_date']
                        ]);
                        if($new_model->save())
                        {
                            $flag=true;
                        }
                        else
                        {
                            $flag=false;
                            break;
                        }
                    }
                }
            }
            if ($flag) {
                $transaction->commit();
                return $this->redirect(['view-all','course_id'=>$course_id]);
            }
            else
            {
                $transaction->rollBack();
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create-all', [
            'model' => $model,
            'student_waiting'=>$student_waiting,
        ]);
    }

    /**
     * Updates an existing CourseRelStudent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        }
        if (Yii::$app->request->isAjax){
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CourseRelStudent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CourseRelStudent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return CourseRelStudent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CourseRelStudent::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    public function actionStudentWaiting()
    {
        Yii::$app->response->format=Response::FORMAT_JSON;
        $course_id=Yii::$app->request->post('id');
        $response['status']=false;
        if($course_id)
        {
            $response['item']=StudentWaiting::getStudentWaitingAll(null,$course_id);
            if(($response['item']) !=null)
            {
                $response['status']=true;
            }
            return $response;
        }
    }
}
