<?php

use kartik\select2\Select2;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\SubjectLevel */
/* @var $form yii\widgets\ActiveForm */
//\yii\helpers\VarDumper::dump($model,10,true);die();
?>

<div class="subject-level-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                
                            <?= $form->field($model, 'subject_id')->widget(Select2::classname(), [
                                'data' =>\app\modules\admin\models\Subject::getSubjects(),
                                'options' => ['placeholder' => 'Select a subject ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]); ?>
                            <div class="card card-body" style="margin-top:35px ">
                                <p>
                                    <?= Html::a(Yii::t('app', 'Create Subject'), ['subject/subject-create'],
                                        ['class' => 'btn btn-success create-button']) ?>
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <?= $form->field($model, 'level_name')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'status')->dropDownList([
                                '1' => 'Active',
                                '0' => 'In_Active'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success ']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
Modal::begin([

    'title' => Yii::t('app', 'Subject'),
    'class' => 'modal',
]);

Modal::end();

$js = <<<JS
    $('body').delegate('.create-button','click',function (event){
        event.preventDefault();
        console.log($(this).attr('href'));
        $('.modal').modal('show').find('.modal-body').load($(this).attr('href'));
    })
    $('body').delegate('.save-button','click',function (event){
        event.preventDefault();
        let form = $(this).parents('form');
        let action = form.attr('action');
        console.log(form.serialize());
        $.ajax({
        url:action,
        type: 'POST',
        data: form.serialize(),
        success: function (response){
            
        }
        })
    })
JS;
$this->registerJs($js);

?>
