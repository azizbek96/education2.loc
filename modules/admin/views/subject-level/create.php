<?php

use yii\bootstrap4\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\SubjectLevel */

$this->title = Yii::t('app', 'Create Subject Level');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Subject Levels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subject-level-create">
    <div class="card">
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
