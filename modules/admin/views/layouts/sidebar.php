<?php

use yii\helpers\Html;
use yii\helpers\Url;

$user=\app\modules\admin\models\SubUsers::find()->where(['user_id'=>\Yii::$app->user->id])->one();
/***
 * @var $assetDir Directory
 */
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="/web/images/education.png" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Education</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <?php
                    if(!($user->image))
                    {
                        ?>
                            <img src="<?=$assetDir?>/img/user2-160x160.jpg" class="img-circle elevation-2" alt="Image">
                        <?php
                    }
                    else
                    {
                        $sourcePath=Url::base().'/uploads/user/'.$user->image;
                        echo Html::img($sourcePath,[
                                'class' => 'img-circle elevation-2',
                                'alt'=>'user image'
                        ]);
                    }
                ?>
            </div>
            <div class="info">
                <a href="#" class="d-block"><?= Yii::$app->user->isGuest ?:Yii::$app->user->identity->username?></a>
            </div>
        </div>


        <nav class="mt-2">
            <?php
            echo \hail812\adminlte\widgets\Menu::widget([
                'items' => [
                    [
                        'label' => 'Adminstrator',
                        'icon' => 'fas fa-users mr-2',
                        'badge' => '<span class="right badge badge-info"></span>',
                        'items' => [
                            [
                                'label' => 'User',
                                'url' => ['users/index'],
                                'iconStyle' => 'fas fa-user',
//                                'visible' => Yii::$app->user->can('users/index'),
                            ],
                            [
                                'label' => 'Employees',
                                'url' => ['employees/index'],
                                'iconStyle' => 'far',
//                                'visible' => Yii::$app->user->can('employees/index'),
                            ],
                            [
                                'label' => 'Subject',
                                'url' => ['subject/index'],
                                'iconStyle' => 'far',
//                                'visible' => Yii::$app->user->can('subject/index'),
                            ],
                            [
                                'label' => 'Subject Level',
                                'url' => ['subject-level/index'],
                                'iconStyle' => 'far',
//                                'visible' => Yii::$app->user->can('subject/index'),
                            ],
                            [
                                'label' => 'User Rel Employee',
                                'url' => ['user-rel-employee/index'],
                                'iconStyle' => 'far',
//                                'visible' => Yii::$app->user->can('user-rel-employee/index'),
                            ],
                            [
                                    'label' => 'Employee Rel Subject',
                                    'url' => ['employees-rel/index'],
                                    'iconStyle' => 'far',
//                                     'visible' => Yii::$app->user->can('employees-rel/index'),
                            ],
                            [
                                    'label' => 'Course',
                                    'url' => ['courses/index'],
                                    'iconStyle' => 'far',
//                                    'visible' => Yii::$app->user->can('courses/index')
                            ],
                            [
                                    'label' => 'Student',
                                    'url' => ['students/index'],
                                    'iconStyle' => 'fas fa-user-graduate',
//                                'visible' => Yii::$app->user->can('students/index')
                            ],
                            [
                                    'label' => 'Student Waiting',
                                    'url' => ['student-waiting/index'],
                                    'iconStyle' => 'far',
//                                'visible' => Yii::$app->user->can('student-waiting/index')
                            ],
                            [
                                    'label' => 'Course Rel Student',
                                    'url' => ['course-rel-student/index'],
                                    'iconStyle' => 'far',
//                                    'visible'=>Yii::$app->user->can('course-rel-student/index')
                            ],
                            [
                                'label' => 'Course Rel Students',
                                'url' => ['course-rel-student/index-all'],
                                'iconStyle' => 'far',
//                                    'visible'=>Yii::$app->user->can('course-rel-student/index-all')
                            ],
                            [
                                'label' => 'Document Kirim',
                                'url' => ['documents/incoming/index'],
                                'iconStyle' => 'far',
//                                    'visible'=>Yii::$app->user->can('documents/incoming/index')
                            ],
                            [
                                'label' => 'Document Outgoing',
                                'url' => ['documents/outgoing/index'],
                                'iconStyle' => 'far',
//                                    'visible'=>Yii::$app->user->can('documents/outgoing/index')
                            ],
                            [
                                'label' => 'Item Balances',
                                'url' => ['item-balances/index'],
                                'iconStyle' => 'far',
//                                    'visible'=>Yii::$app->user->can('item-balances/index')
                            ],
                            [
                                    'label' => 'Role', 'url' => ['auth-item/rule'], 'iconStyle' => 'far',
//                                'visible' => Yii::$app->user->can('auth-item/rule')
                            ],
                            ['label' => 'Permission', 'url' => ['auth-item/permission'], 'iconStyle' => 'far',
//                                'visible' => Yii::$app->user->can('auth-item/permission')
                                ],
                        ]
                    ],
//                    [
//                        'label' => 'Spravishnik', 'icon' => 'fas fa-book', 'badge' => '<span class="right badge badge-danger"></span>',
//                        'items' => [
//
//                            ['label' => 'Course', 'url' => ['courses/index'],'iconStyle' => 'fas fa-clipboard-list'],
//                            ['label' => 'Course Date', 'url' => ['course-date/index'],'iconStyle' => 'fas fa-clock'],
//                            ['label' => 'Documents', 'url' => ['documents/index'],'iconStyle' => 'fas fa-file'],
//                            ['label' => 'DocumentItems', 'url' => ['document-items/index'],'iconStyle' => 'fas fa-file-contract'],
//                            ['label' => 'Course Rel Student', 'url' => ['course-rel-student/index'],'iconStyle' => 'fas fa-user-clock'],
//                        ]
//                    ],
//                    [
//                        'label' => 'Spravishnik', 'icon' => 'fas fa-book', 'badge' => '<span class="right badge badge-danger"></span>',
//                        'items' => [
//
//
//                            ['label' => 'User Rel Employee', 'url' => ['user-rel-employee/index'], 'iconStyle' => 'fas fa-user'],
//                            ['label' => 'Attendance', 'url' => ['attendance/index'],'iconStyle' => 'far'],
//                            ['label' => 'AttendanceItem', 'url' => ['attendance-item/index'],'iconStyle' => 'far'],
//                            ['label' => 'Employee Rel Subject', 'url' => ['employees-rel/index'], 'iconStyle' => 'far'],
//                            ['label' => 'Documents', 'url' => ['documents/index'],'iconStyle' => 'fas fa-file'],
//                            ['label' => 'DocumentItems', 'url' => ['document-items/index'],'iconStyle' => 'fas fa-file-contract'],
//                            ['label' => 'ItemBalances', 'url' => ['item-balances/index'],'iconStyle' => 'fas fa-money-bill'],
//                            ['label' => 'Role', 'url' => ['student/index'], 'iconStyle' => 'far'],
//                            ['label' => 'Permission', 'url' => ['student/index'], 'iconStyle' => 'far'],
//                        ]
//                    ],
//                    ['label' => 'Setting',  'icon' => 'fal fa-cog', 'url' => ['sub-users/index']],
                ],
            ]);
            ?>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
