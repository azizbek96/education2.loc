<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\AttendanceItem */

$this->title = Yii::t('app', 'Create Attendance Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Attendance Items'), 'url' => ['index']];
?>
<div class="attendance-item-create">
    <div class="card">
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
