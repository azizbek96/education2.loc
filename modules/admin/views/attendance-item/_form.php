<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\AttendanceItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attendance-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'attendance_id')->widget(Select2::classname(), [
                'data' => \app\modules\admin\models\Attendance::getAttendaceName(),
                'options' => ['placeholder' => 'Select a attendance ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>

            <?= $form->field($model, 'type')->dropDownList([
                '1'=>1,
                '2'=>2
            ]) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'student_id')->widget(Select2::classname(), [
                'data' => \app\modules\admin\models\Students::getStudents(),
                'options' => ['placeholder' => 'Select a course ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>

            <?= $form->field($model, 'status')->dropDownList($model->getStatus()) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
