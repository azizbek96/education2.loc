<?php

use kartik\editors\Codemirror;
use kartik\editors\Summernote;use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Position */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="position-form">


    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatus()) ?>

    <?= $form->field($model, 'description')->textarea([
            'rows'=>7,
            'style' =>'resize:off']);?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success save-button' ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
