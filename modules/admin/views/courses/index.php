<?php

use app\modules\admin\models\Courses;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\CoursesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Courses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="courses-index">

    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('app', 'Create Courses'), ['create'], ['class' => 'btn btn-success create-button']) ?>
            </p>

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                 'layout'=>'{items} {pager}',
                 'rowOptions' =>function($data)
                     {
                             if(!($data->status == 1))
                             {
                                 return ['class'=>'table-danger'];
                             }
                             return ['class' => 'table-success'];
                     },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
                    [
                        'attribute' =>'subject_id',
                        'value' =>function($model)
                        {
                            if(!($model->subject->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->subject->name."</button>";
                            }
                            return $model->subject->name;
                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute' =>'employee_id',
                        'value' =>function($model)
                        {
                            if(!($model->employee->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->employee->first_name."</button>";
                            }
                            return $model->employee->first_name;
                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute' =>'subject_level_id',
                        'value' =>function($model)
                        {
                            if(!($model->subjectLevel->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->subjectLevel->level_name."</button>";
                            }
                            return $model->subjectLevel->level_name;
                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute'=>'status',
                        'value'=>function($data){
                            if($data->status == 0)
                            {
                                return "<button class='btn  btn-danger'>In_Active</button>";
                            }
                            return "<button class='btn  btn-primary'>Active</button>";
                        },
                        'format' => 'raw'
                    ],
                    'updated_by',
                    [
                        'attribute' => 'updated_at',
                        'value' => function($data){
                            // time utf formatda boladi 12212121 korinishida
                            $d1=new DateTime(date('Y-m-d H:i:s'));
                            $d2=new DateTime(date('Y-m-d H:i:s',$data->updated_at));
                            $diff=$d1->diff($d2);
                            return "<code>".$diff->format('%Y-%m-%d %H:%i:%s')."</code>";
                        },
                        'format'=>'raw',
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'buttons' => [
                            'view' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-eye"></span>',['view', 'id'=>$model->id],[
                                    'title'=>Yii::t('app','view'),
                                    'class' => 'btn btn-xs btn-info view-button'
                                ]);
                            },
                            'update' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-pen"></span>',['update', 'id'=>$model->id],[
                                    'title'=>Yii::t('app','update'),
                                    'class' => 'btn btn-xs btn-primary update-button'
                                ]);
                            },
                            'delete' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-trash"></span>',['delete', 'id'=>$model->id],[
                                    'title'=>Yii::t('app','delete'),
                                    'class' => 'btn btn-xs btn-danger',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' =>'Are you sure ?'
                                    ]
                                ]);
                            }
                        ],
                        'template' => '{update} {view} {delete}',
                        'contentOptions' => ['style' => 'width:110px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>