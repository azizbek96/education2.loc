<?php

use app\modules\admin\models\Subject;
use kartik\date\DatePicker;
use kartik\daterange\DateRangePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Courses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="courses-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'employee_id')->widget(Select2::className(),[
                'data' => \app\modules\admin\models\Employees::getEmployeeAll(),
                'options' => ['placeholder'=> 'tanlang...'],
                'pluginOptions' => ['allowClear' => true],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'subject_id')->dropDownList(Subject::getSubjects(), [
                'prompt'=>'select a subject...',
                'id'=>'subject_id']);?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'subject_level_id')->widget(DepDrop::classname(), [
                'options'=>['id'=>'subject_level'],
                'pluginOptions'=>[
                    'depends'=>['subject_id'],
                    'placeholder'=>'Select a subject a level...',
                    'url'=>Url::to(['courses/subject'])
                ]
            ]);?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">

            <?php echo $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                'name' => 'start_date',
                'value'=>date('Y-m-d'),
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pickerIcon' => '<i class="fas fa-calendar-alt text-primary"></i>',
                'removeIcon' => '<i class="fas fa-trash text-danger"></i>',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'end_date')->widget(DatePicker::className(),[
                'name' => 'end_date',
                'value'=>date('Y-m-d'),
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pickerIcon' => '<i class="fas fa-calendar-alt text-primary"></i>',
                'removeIcon' => '<i class="fas fa-trash text-danger"></i>',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
                if(!($model->isNewRecord))
                {
                    echo $form->field($model, 'status')->dropDownList($model->getStatus());
                }
            ?>
        </div>
    </div>
    <div class="col-md-12">
        <?php
            echo $form->field($model, 'course_date')->widget(MultipleInput::className(), [
                'max'=> 7,
                'min'=> 1,
                'addButtonOptions' => [
                    'class' => 'btn btn-success',
                    'label' => '<i class="fas fa-plus">' // also you can use html code
                ],
                'removeButtonOptions' => [
                    'class'=>'btn btn-danger',
                    'label' => '<i class="fas fa-trash-alt">'
                ],
                'cloneButtonOptions'=>[
                    'class' => 'fas fa-trash'
                ],
                'columns'=>[
                        [
                            'name' => 'type',
                            'title'=>Yii::t('app','Week'),
                            'type' => Select2::class,
                            'defaultValue' => '',
                            'headerOptions' =>[
                                'style' => 'width: 532px'
                            ],
                            'options'=>[
                                    'data'=>$model::getWeeks(),
                                    'options'=>[
                                        'placeholder'=> Yii::t('app','Select a day ....')
                                    ],
                            ]
                        ],
                        [
                            'name' => 'course_time',
                            'title'=>Yii::t('app','Izoh'),
                            'type' => \kartik\time\TimePicker::class,
                            'options' => [
                                'pluginOptions' => [
                                    'showSeconds' => false,
                                    'showMeridian' => false,
                                    'defaultTime' => ''
                                ]
                            ]
                        ]
                ]
            ])->label(false);
        ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
