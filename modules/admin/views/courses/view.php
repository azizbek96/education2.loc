<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Courses */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Courses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="courses-view">

    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    [
                            'attribute' =>'subject_id',
                            'value' =>function($model)
                            {
                                return $model->subject->name;
                            }
                    ],
                    [
                        'attribute' =>'employee_id',
                        'value' =>function($model)
                        {
                            return $model->employee->first_name;
                        }
                    ],
                    [
                        'attribute' =>'subject_level_id',
                        'value' =>function($model)
                        {
                            return $model->subjectLevel->level_name;
                        }
                    ],
                    'start_date',
                    'end_date',
                    [
                        'attribute'=>'status',
                        'value'=>function($data){
                            if($data->status == 0)
                            {
                                return "<button class='btn btn-xs btn-danger'>In_Active</button>";
                            }
                            return "<button class='btn btn-xs btn-primary'>Active</button>";
                        },
                        'format' => 'raw'
                    ],
//                    'created_by',
                    'updated_by',
//                    'created_at',
                    [
                        'attribute' => 'updated_at',
                        'value' => function($data){
                            // time utf formatda boladi 12212121 korinishida
                            $d1=new DateTime(date('Y-m-d H:i:s'));
                            $d2=new DateTime(date('Y-m-d H:i:s',$data->updated_at));
                            $diff=$d1->diff($d2);
                            return "<code>".$diff->format('%Y-%m-%d %H:%i:%s')."</code>";
                        },
                        'format'=>'raw',
                    ],
                ],
            ]) ?>
        </div>
    </div>

</div>
