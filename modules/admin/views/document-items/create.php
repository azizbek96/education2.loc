<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\DocumentItems */

$this->title = Yii::t('app', 'Create Document Items');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Document Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-items-create">
    <div class="card">
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
