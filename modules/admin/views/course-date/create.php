<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\CourseDate */

$this->title = Yii::t('app', 'Create Course Date');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Course Dates'), 'url' => ['index']];
?>
<div class="course-date-create">
    <div class="card">
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
