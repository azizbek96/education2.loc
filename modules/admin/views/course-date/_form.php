<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\time\TimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\CourseDate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-date-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->textInput() ?>

    <?= $form->field($model, 'course_id')->widget(Select2::class,[
            'data'=>\app\modules\admin\models\Courses::getCoursesName()
    ]) ?>

    <?= $form->field($model, 'course_time')->widget(TimePicker::classname(), [
        'name' => 't3a',
        'size' => 'sm',
        'containerOptions' => ['class' => 'has-success']
    ]); ?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatus()) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
