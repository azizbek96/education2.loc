<?php

use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Employees */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="employees-form">

    <?php $form = ActiveForm::begin([
            'options'=>[
                    'enctype' => 'multipart/form-data',
            ]
    ]); ?>

    <div class="row" style="margin-bottom: 100px">
        <div class="col-lg-8">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'age')->input('number',['min' => 10]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true,'placeholder'=>'example@gmail.com']) ?>

            <?php echo $form->field($model, 'phone')->widget(MaskedInput::class,[
                    'mask'=>'+\9\9\8 99-999-99-99'
            ]); ?>

            <?= $form->field($model, 'status')->dropDownList($model->getStatus(),['placeholder'=>'statusni tanlang']) ?>

            <div class="card">
                <div class="card-body" style="margin-top:15px; ">
                    <a href=""><label for="employees-status">Select Position</label></a>
                    <?= $form->field($model, 'position_id')->widget(Select2::classname(), [
                        'data' => $model->getPositionId(),
                        'options' => ['placeholder' => 'Select a state ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false); ?>
                </div>
            </div>
            <div class="card card-body" style="margin-top:35px ">
                <p>
                    <a class="card-link" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <span style="font-size: 15px">Create new position <i class="text-bold" style="font-size: 20px">+</i></span>
                    </a>
                </p>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                        <?php echo $form->field($model, 'pos_name')->textInput(['placeholder'=>'position name...']); ?>

                        <?php echo $form->field($model, 'pos_description')->textarea(['rows'=>5,'style' => [ 'resize' => 'none']]); ?>
                    </div>
                </div>
            </div>



            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model,'imageFile')->widget(FileInput::class,[
                'name' => 'imageFile',
                'options'=>[
                    'multiple'=>false
                ],
                'pluginOptions' => [
                    'initialPreview'=>[
                        $model->photo ? '/uploads/image/'.$model->photo : null ,
                    ],
                ]
            ])?>

            <?= $form->field($model,'exeFile')->widget(FileInput::class,[
                'name' => 'exe',
                'options'=>[
                    'multiple'=>false
                ],
                'pluginOptions' => [
                    'initialPreview'=>[
                        $model->photo ? '/uploads/image/'.$model->photo : null ,
                    ],
                ]
            ])?>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
