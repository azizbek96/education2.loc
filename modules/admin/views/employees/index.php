<?php

use app\modules\admin\models\Employees;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\EmployeesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Employees');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employees-index">
        <?php
            \yii\bootstrap4\Modal::begin([
                    'id' => 'employee_view_modal'
            ]);
        ?>
            <div id="employee_view_page"></div>
        <?php
            \yii\bootstrap4\Modal::end();
        ?>

    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('app', 'Create Employees'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => '{items} {pager}',
                 'rowOptions' =>function($data)
                     {
                             if(!($data->status == 1))
                             {
                                 return ['class'=>'table-danger'];
                             }
                             return ['class' => 'table-success'];
                     },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                            'attribute' => 'first_name',
                            'headerOptions' => ['style' => ['width' => '150px']]
                    ],
                    [
                        'attribute' => 'last_name',
                        'headerOptions' => ['style' => ['width' => '150px']]
                    ],
                    [
                            'attribute'=>'position_id',
                            'label'=>'Position',
                            'value'=>'position.name'
                    ],
                    'phone',
                    [
                        'attribute' => 'photo',
                        'format'=>'raw',
                        'value' => function($data)
                        {
                            return Html::img('/uploads/image/'.$data->photo,['width'=>'100px',
                                'style' => [
                                        'margin-left' => '70px'
                                ]
                            ]);
                        }
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'buttons' => [
                            'view' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-eye"></span>',$url,[
                                    'class' => 'btn btn-xs btn-light',
                                    'id'=>'employee_view'
                                ]);
                            },
                            'update' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-pen"></span>',$url,[
                                    'class' => 'btn btn-xs btn-primary'
                                ]);
                            },
                            'delete' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-trash"></span>',$url,[
                                    'class' => 'btn btn-xs btn-danger',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' =>'Are you sure ?'
                                    ]
                                ]);
                            }
                        ],
                        'template' => '{update} {view} {delete}'
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>
        </div>
    </div>

</div>
<?php
$script = <<< JS
     $(document).ready(function (){
            $("#employee_view").on('click',function (e) {
            e.preventDefault();
            $("#employee_view_page").load($(this).attr("href"));
            $("#employee_view_modal").modal("show");
    })
     })
JS;
$this->registerJs($script);
?>