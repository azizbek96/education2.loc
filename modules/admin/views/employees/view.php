<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Employees */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Employees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="employees-view">
    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
//                    'id',
                    'first_name',
                    'last_name',
                    'age',
                    'phone',
                    [
                        'attribute' => 'photo',
                        'format'=>'raw',
                        'value' => function($data)
                        {
                            return Html::img('/uploads/image/'.$data->photo,['width'=>'100px',
                            ]);
                        }
                    ],
                    'email:email',
                    'file',
                    'position_id',
                    'status',
                    [
                        'attribute'=>'created_at',
                        'value' => function($data)
                        {
                            return date('Y-m-d H:i:s', $data->created_at);
                        }
                    ],
                    [
                        'attribute'=>'updated_at',
                        'value' => function($data)
                        {
                            return date('Y-m-d H:i:s', $data->updated_at);
                        }
                    ],
//                    'created_by',
//                    'updated_by',
                ],
            ]) ?>
        </div>
    </div>


</div>
