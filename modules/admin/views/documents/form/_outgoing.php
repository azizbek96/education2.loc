<?php


use app\modules\admin\models\Documents;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Documents */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsVar('employee_url',Url::to(['documents/outgoing/employee']));
$this->registerJsVar('course_url',Url::to(['documents/outgoing/course']));
?>
<div class="row">
    <?= $form->field($model, 'document_type')->hiddenInput(['value'=>Documents::DOC_TYPE_OUTGOING])->label(false) ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'document_number')->textInput(['maxlength' => true,'readOnly'=>true]) ?>

        <?= $form->field($model, 'employee_id')->widget(\kartik\select2\Select2::class,[
            'data'=>\app\modules\admin\models\Employees::getEmployeeList(),
            'pluginOptions'=>[
                'placeholder'=>'Select a employee',
                'allowClear'=>true
            ],
            'pluginEvents'=>[
                'change'=>"function(){
                        let id=$(this).val();
                        $.ajax({
                            url:employee_url,
                            type:'POST',
                            data:{id:id},
                            success:function(response){
                                let course_id=$('#documents-course_id');
                                course_id.html('');
                                if(response.status){
                                    course_id.removeAttr('disabled');
                                    course_id.append(`<option >select a course</option>`)
                                    response.items.map((item,index)=>{
                                        let newOption=new Option(item.name,item.id);
                                        course_id.append(newOption);
                                    })
                                }
                                else
                                { alert('error')}
                            }
                        })
                    }"
            ]
        ]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'course_id')->widget(\kartik\select2\Select2::class,[
            'data'=>'',
            'pluginOptions'=>[
                'placeholder'=>'select a course',
                'disabled'=>true,
            ],
            'pluginEvents'=>[
                'change' => "function(){
                            let id=$(this).val();
                            $.ajax({
                                url:course_url,
                                type:'POST',
                                data:{id:id},
                                success:function(response){
                                    let student_id=$('.multiple-input-list__item select');
                                    student_id.html('');
                                    if(response.status){                                          
                                        student_id.append('<option>select</option>');
                                        response.items.map((item,index)=>{
                                            let newOption=new Option(item.fullname,item.student_id);
                                            student_id.append(newOption);
                                        })
                                    }                                                
                                }
                            })
                        }"
            ]

        ]) ?>

        <?= $form->field($model, 'reg_date')->widget(DateTimePicker::class,[
            'pluginOptions'=>[
                'autoclose' => true,
                'format'=>'yyyy-mm-dd hh:ii'
            ]
        ]) ?>
    </div>
    <div class="col-lg-12">
        <?= $form->field($model, 'add_info')->textarea(['rows' => 2,
            'style'=>['resize'=>'none']]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?php
        echo MultipleInput::widget([
//            'max'=> 7,
//            'min'=> 1,
            'name'=>'DocumentItems',
            'addButtonOptions' => [
                'class' => 'btn btn-success',
                'label' => '<i class="fas fa-plus">' // also you can use html code
            ],
            'removeButtonOptions' => [
                'class'=>'btn btn-danger',
                'label' => '<i class="fas fa-trash-alt">'
            ],
            'cloneButtonOptions'=>[
                'class' => 'fas fa-trash'
            ],
            'columns'=>[
                [
                    'name' => 'student_id',
                    'title'=>Yii::t('app','student_id'),
                    'type' => Select2::class,
                    'headerOptions' =>[
                        'style' => 'width: 300px'
                    ],
                    'options'=>[
                        'data'=>'',
                        'options'=>[
                        ],
                    ]
                ],
                [
                    'name' => 'price',
                    'title'=>Yii::t('app','Narxi'),
                    'type' => \kartik\money\MaskMoney::class,
                    'options' => [
                        'value' => null,
                        'options' => [
                            'placeholder' => 'Enter a valid amount...'
                        ],
                        'pluginOptions' => [
                            'prefix' => '$ ',
                            'precision' => 0
                        ]
                    ]
                ],
                [
                    'name' => 'price_date',
                    'title'=>Yii::t('app','Berilgan Sana'),
                    'type' => DatePicker::class,
                    'options' => [
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format'=>'yyyy-mm-dd'
                        ]
                    ]
                ],
                [
                    'name' => 'add_info',
                    'title'=>Yii::t('app','Izoh'),
                    'type'=>'textarea',
                    'options'=>[
                        'style'=>'resize:none',
                        'rows'=>1
                    ]
                ]
            ]
        ]);
        ?>
    </div>
</div>









