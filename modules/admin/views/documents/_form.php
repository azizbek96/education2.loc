<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Documents */
/* @var $models app\modules\admin\models\DocumentItems */
/* @var $form yii\widgets\ActiveForm */

$slug=$this->context->slug;
?>

<div class="documents-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $this->render('form/_'.$slug, [
        'model' => $model,
        'models'=>$models,
        'form'=>$form,
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
