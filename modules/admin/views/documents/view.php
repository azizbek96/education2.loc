<?php

use app\modules\admin\models\Documents;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Documents */
/* @var $models app\modules\admin\models\DocumentItems */
$slug=$this->context->slug;
$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Documents  '.Documents::getDocTypeBySlug($slug)), 'url' => ['index','slug'=>$slug]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="documents-view">
    <div class="card">
        <div class="card-body">
            <?= $this->render('views/_'.$slug, [
                'model' => $model,
                'models'=>$models
            ]) ?>
        </div>
    </div>
</div>
