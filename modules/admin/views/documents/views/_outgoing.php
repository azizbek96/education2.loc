<?php

use app\modules\admin\models\Documents;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Documents */
/* @var $models app\modules\admin\models\DocumentItems */
/* @var $searchModel */
$slug=$this->context->slug;
?>

<p>
    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'slug' => $slug], ['class' => 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id, 'slug' => $slug], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
</p>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
//        'id',
        'course.name',
        'employee.first_name',
        'reg_date',
        'document_number',
        'document_type',
        'add_info:ntext',
    ],
]) ?>
<br><br>
<?= GridView::widget([
    'dataProvider' => $models,
    'layout'=>'{items} {pager}',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'student.first_name',
        'price',
        'price_date',
    ],
]); ?>
