<?php

use app\modules\admin\models\Documents;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\DocumentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Documents');
$this->params['breadcrumbs'][] = $this->title;
$slug=$this->context->slug;
$title_slug=Documents::getDocTypeBySlug($this->context->slug);
?>
<div class="documents-index">
    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('app', 'Create Documents '.$slug), ['create','slug'=>$slug], ['class' => 'btn btn-success']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout'=>'{items} {pager}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

//                'id',
                    'document_number',
                    'document_type',
                    'course_id',
                    'employee_id',
                    'reg_date',
                    [
                        'attribute'=>'status',
                        'value'=>function($data){
                            if($data->status == 0)
                            {
                                return "<button class='btn  btn-danger'>In_Active</button>";
                            }
                            return "<button class='btn  btn-primary'>Active</button>";
                        },
                        'format' => 'raw'
                    ],
                    'add_info',
                    [
                        'attribute' => 'updated_at',
                        'value' => function($data){
                            // time utf formatda boladi 12212121 korinishida
                            $d1=new DateTime(date('Y-m-d H:i:s'));
                            $d2=new DateTime(date('Y-m-d H:i:s',$data->updated_at));
                            $diff=$d2->diff($d1);
                            return "<code>".$diff->format('%m-%d %H:%i:%s')."</code>";
                        },
                        'format'=>'raw',
                    ],

                    [
                        'class' => ActionColumn::className(),
                        'buttons' => [
                            'view' => function($url,$model,$index)use($slug){
                                return Html::a('<span class="fa fa-eye"></span>',[$url,'slug'=>$slug],[
                                    'class' => 'btn btn-xs btn-light view-button',
                                    'id' => 'position_view'
                                ]);
                            },
                            'update' => function($url,$model,$index)use($slug){
                                return Html::a('<span class="fa fa-pen"></span>',[$url,'slug'=>$slug],[
                                    'class' => 'btn btn-xs btn-primary update-button'
                                ]);
                            },
                            'delete' => function($url,$model,$index)use($slug){
                                return Html::a('<span class="fa fa-trash"></span>',[$url,'slug'=>$slug],[
                                    'class' => 'btn btn-xs btn-danger delete-button',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' =>'Are you sure ?'
                                    ]
                                ]);
                            }
                        ],
                        'template' => '{update} {view} {delete}',
                        'headerOptions'=>[
                                'style'=>'width:100px',
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
