<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Documents */
/* @var $models app\modules\admin\models\DocumentItems */

$this->title = Yii::t('app', 'Create Documents '.$this->context->slug);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Documents'), 'url' => ['index','slug'=>$this->context->slug]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documents-create">
    <div class="card">
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
                'models'=>$models
            ]) ?>
        </div>
    </div>
</div>
