<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $type */
/* @var $model app\modules\admin\models\AuthItem */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Auth Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="auth-item-view">


    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'name' => $model->name, 'type'=>$type], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'name' => $model->name, 'type'=>$type], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    [
                        'attribute'=>'type',
                        'label'=>'Type',
                        'value'=> function($model){
                            return \app\modules\admin\models\AuthItem::getList($model->type);
                        }
                    ],
                    'description:ntext',
//                    'rule_name',
                    'data',
                    'created_at',
                    'updated_at',
                ],
            ]) ?>
        </div>
    </div>


</div>
