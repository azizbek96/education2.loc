<?php

use app\modules\admin\models\AuthItem;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
//use yii\bootstrap5\ActiveForm

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\AuthItem */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="auth-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if ($model->type==AuthItem::AUTH_ITEM_RULE): ?>


            <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?php echo $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?php echo $form->field($model, 'data')->textInput() ?>

    <?php endif; ?>

    <?php if ($model->type==AuthItem::AUTH_ITEM_PERMISSION): ?>

        <?php echo $form->field($model, 'controller')->textInput(['maxlength' => true])->label('name') ?>

        <?= $form->field($model, 'new_permissions')->widget(MultipleInput::className(), [
            'max' => 6,
            'min' => 0,
            'cloneButton' => true,
            'addButtonOptions' => [
                'class' => 'btn btn-success',
                'label' => 'add' // also you can use html code
            ],
            'removeButtonOptions' => [
                'class' => 'fa fa-window-close btn btn-danger ',
                'style' => 'color:black',
            ],
            'cloneButtonOptions' =>[
                 'class' => 'fa fa-clone btn btn-info',
                'iconMap' => 'fa fa-clone',
            ],
            'columns' => [
                [
                    'name'  => 'name',
                    'title' => 'Name',
                    'defaultValue' => '',
                ],
                [
                    'name'  => 'description',
                    'title' => 'Izoh',
                ],

            ],
        ])->label('Permissions');
        echo $form->field($model, 'category')->widget(Select2::classname(), [
            'data' => AuthItem::getListRule(),
            'options' => ['placeholder' => Yii::t('app', 'Select_Category'),],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
