<?php

use app\modules\admin\models\AuthItem;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\AuthItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Auth Items');
$this->params['breadcrumbs'][] = $this->title;
$type = !is_null($searchModel->is_type)?($searchModel->is_type===1)?'Rule':'Permission':'';
?>
<div class="auth-item-index">


    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('app', 'Create ').$type, ['create','type'=>$searchModel->is_type], ['class' => 'btn btn-success']) ?>
            </p>

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
//                    'type',
                    'description:ntext',
//                    'rule_name',
                    'data',
                    //'created_at',
                    //'updated_at',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {view} {delete}',
                        'contentOptions' => ['class' => 'no-print', 'style' => 'width:130px'],
                        'buttons' => [
                            'update' => function ($url, $model){
                                return Html::a('<span class="fa fa-edit"></span>',
                                    ['update','name'=>$model->name, 'type'=>$model->type],
                                    ['title' => Yii::t('app','Update'),
                                    'class' => 'btn btn-xs btn-success'
                                ]);
                            },
                            'view' => function ($url, $model){
                                return Html::a('<span class="fa fa-eye"></span>',
                                    ['view','name'=>$model->name, 'type'=>$model->type],
                                    ['title' => Yii::t('app','View'),
                                    'class' => 'btn btn-xs btn-default'
                                ]);
                            },
                            'delete' => function ($url, $model){
                                return Html::a('<span class="fa fa-trash"></span>',
                                    ['delete','name'=>$model->name,'type'=>$model->type],
                                    ['title' => Yii::t('app','Delete'),
                                    'class' => 'btn btn-xs btn-danger',
                                    'data' => [
                                        'confirm' => Yii::t('app','Are you sure'),
                                        'method' => 'post',
                                    ]
                                ]);
                            },
                            ]
                    ],
                ],
            ]); ?>
        </div>
    </div>


</div>
