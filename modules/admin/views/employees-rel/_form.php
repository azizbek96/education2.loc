<?php

use app\modules\admin\models\Employees;
use app\modules\admin\models\Subject;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\EmployeeRelSubject */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="employee-rel-subject-form">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'id' => 'subject-form',
                        ]
                    ]); ?>


                    <?= $form->field($model, 'subject_id')->widget(Select2::classname(), [
                        'data' =>Subject::getSubjectId(),
                        'options' => ['placeholder' => 'Select a subject ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                    <div class="card">
                        <div class="card-body">
                            <p>
                                <button class="btn btn-success" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    <i class="fas fa-plus"></i>  Add New Subject
                                </button>
                            </p>
                            <div class="collapse" id="collapseExample">
                                <div class="card card-body">

                                    <?= $form->field($model,'sub_name')->textInput() ?>

                                    <?= $form->field($model,'sub_level')->textInput() ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?= $form->field($model, 'status')->dropDownList($model->getStatus()) ?>

                </div>
                <div class="col-lg-6">
                    <?= $form->field($model, 'employee_id')->widget(Select2::classname(), [
                        'data' =>Employees::getEmployeeAll(),
                        'options' => ['placeholder' => 'Select a employee ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>

                    <?php echo $form->field($model, 'description')->textarea(['rows'=>5,'style' => [ 'resize' => 'none']]); ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>
