<?php

use app\modules\admin\models\EmployeeRelSubject;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\EmployeeRelSubjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employee Rel Subjects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-rel-subject-index">
    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a('Create Employee Rel Subject', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout'=>'{items} {pager}',
                 'rowOptions' =>function($data)
                     {
                             if(!($data->status == 1))
                             {
                                 return ['class'=>'table-danger'];
                             }
                             return ['class' => 'table-success'];
                     },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' =>'subject_id',
                        'value' =>function($model)
                        {
                            if(!($model->subject->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->subject->name."</button>";
                            }
                            return $model->subject->name;
                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute' =>'employee_id',
                        'value' =>function($model)
                        {
                            if(!($model->employee->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->employee->first_name.' '.$model->employee->last_name."</button>";
                            }
                            return $model->employee->first_name.' '.$model->employee->last_name;
                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute'=>'status',
                        'value'=>function($data){
                            if($data->status == 0)
                            {
                                return "<button class='btn  btn-danger'>In_Active</button>";
                            }
                            return "<button class='btn  btn-primary'>Active</button>";
                        },
                        'format' => 'raw'
                    ],
                    'updated_by',
                    [
                        'attribute' => 'updated_at',
                        'value' => function($data){
                            // time utf formatda boladi 12212121 korinishida
                            $d1=new DateTime(date('Y-m-d H:i:s'));
                            $d2=new DateTime(date('Y-m-d H:i:s',$data->updated_at));
                            $diff=$d1->diff($d2);
                            return "<code>".$diff->format('%Y-%m-%d %H:%i:%s')."</code>";
                        },
                        'format'=>'raw',
                    ],
                    'description',

                    [
                        'class' => ActionColumn::className(),
                        'buttons' => [
                            'view' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-eye"></span>',$url,[
                                    'class' => 'btn btn-xs btn-light'
                                ]);
                            },
                            'update' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-pen"></span>',$url,[
                                    'class' => 'btn btn-xs btn-primary'
                                ]);
                            },
                            'delete' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-trash"></span>',$url,[
                                    'class' => 'btn btn-xs btn-danger',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' =>'Are you sure ?'
                                    ]
                                ]);
                            }
                        ],
                        'template' => '{update} {view} {delete}',
                        'contentOptions'=>[
                                'style' => ['width'=>'120px']
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
