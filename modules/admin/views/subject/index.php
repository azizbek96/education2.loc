<?php

use app\modules\admin\models\Subject;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\SubjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subjects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subject-index">
    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a('Create Subject', ['create'], ['class' => 'btn btn-success create-button']) ?>
            </p>


            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => '{items} {pager}',
                 'rowOptions' =>function($data)
                     {
                             if(!($data->status == 1))
                             {
                                 return ['class'=>'table-danger'];
                             }
                             return ['class' => 'table-success'];
                     },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

//                    'id',
                    'name',
                    'level',
                    [
                        'attribute'=>'status',
                        'value'=>function($data){
                            if($data->status == 0)
                                {
                                    return "<button class='btn  btn-danger'>In_Active</button>";
                                }
                            return "<button class='btn  btn-primary'>Active</button>";
                        },
                        'format' => 'raw'
                    ],
                    'updated_by',
                    [
                        'attribute' => 'updated_at',
                        'value' => function($data){
                            // time utf formatda boladi 12212121 korinishida
                            $d1=new DateTime(date('Y-m-d H:i:s'));
                            $d2=new DateTime(date('Y-m-d H:i:s',$data->updated_at));
                            $diff=$d1->diff($d2);
                            return "<code>".$diff->format('%Y-%m-%d %H:%i:%s')."</code>";
                        },
                        'format'=>'raw',
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'buttons' => [
                            'view' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-eye"></span>',$url,[
                                    'class' => 'btn btn-xs btn-success view-button'
                                ]);
                            },
                            'update' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-pen"></span>',$url,[
                                    'class' => 'btn btn-xs btn-primary update-button'
                                ]);
                            },
                            'delete' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-trash"></span>',$url,[
                                    'class' => 'btn btn-xs btn-danger',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' =>'Are you sure ?'
                                    ]
                                ]);
                            }
                        ],
                        'template' => '{update} {view} {delete}',
                        'headerOptions'=>[
                                'width'=>'120px'
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<?php
Modal::begin([
    'title' => Yii::t('app','Create Position'),
    'id' => 'position-modal'
]);

Modal::end();
$js=<<<JS
        $('body').delegate('.view-button,.create-button, .update-button','click',function(e) {
            e.preventDefault();
            $('#position-modal').modal('show').find('.modal-body').load($(this).attr('href'));
        })
        $('body').delegate('.save-button','submit',function(e){
           e.preventDefault();
           let form = $(this).parents('form');
           let action = form.attr('action');
            $.ajax({
                url:action,
                type: 'POST',
                data: form.serialize(),
                success: function (response){
                    
                }
            })
        })
    JS;
$this->registerJs($js);
?>