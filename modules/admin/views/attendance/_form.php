<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Attendance */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attendance-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'course_id')->widget(Select2::classname(), [
                'data' => \app\modules\admin\models\Courses::getCoursesName(),
                'options' => ['placeholder' => 'Select a course ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>

            <?= $form->field($model, 'status')->dropDownList($model->getStatus()) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'employee_id')->widget(Select2::classname(), [
                'data' => \app\modules\admin\models\Employees::getEmployeesName(),
                'options' => ['placeholder' => 'Select a employee ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>

            <?= $form->field($model, 'attendance_date')->widget(DatePicker::className(),[
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'options' => ['placeholder' => 'attendance_Date ...'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
