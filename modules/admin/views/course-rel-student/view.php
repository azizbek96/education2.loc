<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\CourseRelStudent */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Course Rel Students'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="course-rel-student-view">

    <div class="card">
        <div class="card-body">
            <p>
<!--                --><?//= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
//                    'id',
                    [
                        'attribute' =>'course_id',
                        'value' =>function($model)
                        {
                            if(!($model->course->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->course->name."</button>";
                            }
                            return $model->course->name;
                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute' =>'student_id',
                        'value' =>function($model)
                        {
                            if(!($model->student->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->student->first_name.' '.$model->student->last_name."</button>";
                            }
                            return $model->student->first_name.' '.$model->student->last_name;
                        },
                        'format'=>'raw'
                    ],
                    'start_date',
                    'end_date',
                    [
                        'attribute'=>'status',
                        'value'=>function($data){
                            if($data->status == 0)
                            {
                                return "<button class='btn  btn-danger'>In_Active</button>";
                            }
                            return "<button class='btn  btn-primary'>Active</button>";
                        },
                        'format' => 'raw'
                    ],
                    'updated_by',
                    [
                        'attribute' => 'updated_at',
                        'value' => function($data){
                            // time utf formatda boladi 12212121 korinishida
                            $d1=new DateTime(date('Y-m-d H:i:s'));
                            $d2=new DateTime(date('Y-m-d H:i:s',$data->updated_at));
                            $diff=$d1->diff($d2);
                            return "<code>".$diff->format('%Y-%m-%d %H:%i:%s')."</code>";
                        },
                        'format'=>'raw',
                    ],
                ],
            ]) ?>
        </div>
    </div>

</div>
