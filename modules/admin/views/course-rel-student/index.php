<?php

use app\modules\admin\models\CourseRelStudent;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\CourseRelStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Course Rel Students');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="course-rel-student-index">


    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('app', 'Create Course Rel Student'), ['create'], ['class' => 'btn btn-success create-button']) ?>
            </p>

            <?php Pjax::begin(); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout'=>'{items} {pager}',
                 'rowOptions' =>function($data)
                     {
                             if(!($data->status == 1))
                             {
                                 return ['class'=>'table-danger'];
                             }
                             return ['class' => 'table-success'];
                     },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' =>'course_id',
                        'value'=>function($model){
                            return $model->course->name.' ('.$model->course->subjectLevel->level_name.' ) ';
                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute' =>'student_id',
                        'value' =>function($model)
                        {
                            if(!($model->student->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->student->first_name.' '.$model->student->last_name."</button>";
                            }
                            return $model->student->first_name.' '.$model->student->last_name;
                        },
                        'format'=>'raw'
                    ],
                    'start_date',
                    'end_date',
                    [
                        'attribute'=>'status',
                        'value'=>function($data){
                            if($data->status == 0)
                            {
                                return "<button class='btn  btn-danger'>In_Active</button>";
                            }
                            return "<button class='btn  btn-primary'>Active</button>";
                        },
                        'format' => 'raw'
                    ],
                    'updated_by',
                    [
                        'attribute' => 'updated_at',
                        'value' => function($data){
                            // time utf formatda boladi 12212121 korinishida
                            $d1=new DateTime(date('Y-m-d H:i:s'));
                            $d2=new DateTime(date('Y-m-d H:i:s',$data->updated_at));
                            $diff=$d1->diff($d2);
                            return "<code>".$diff->format('%Y-%m-%d %H:%i:%s')."</code>";
                        },
                        'format'=>'raw',
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'buttons' => [
                            'view' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-eye"></span>',['view', 'id'=>$model->id],[
                                    'title'=>Yii::t('app','view'),
                                    'class' => 'btn btn-xs btn-info view-button'
                                ]);
                            },
                            'update' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-pen"></span>',['update', 'id'=>$model->id],[
                                    'title'=>Yii::t('app','update'),
                                    'class' => 'btn btn-xs btn-primary update-button'
                                ]);
                            },
                            'delete' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-trash"></span>',['delete', 'id'=>$model->id],[
                                    'title'=>Yii::t('app','delete'),
                                    'class' => 'btn btn-xs btn-danger',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' =>'Are you sure ?'
                                    ]
                                ]);
                            }
                        ],
                        'template' => '{update} {view} {delete}',
                        'contentOptions' => ['style' => 'width:110px']
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</div>
<?php
Modal::begin([

    'title' => Yii::t('app', 'Subject Level'),
    'class' => 'modal',
    'size' => Modal::SIZE_EXTRA_LARGE,
]);

Modal::end();

$js = <<<JS
    $('body').delegate('.view-button ,.update-button,.create-button','click',function (event){
        event.preventDefault();
        console.log($(this).attr('href'));
        $('.modal').modal('show').find('.modal-body').load($(this).attr('href'));
    })
   
JS;
$this->registerJs($js);

?>