<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\CourseRelStudent */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsVar('url',Url::to(['student-waiting']));
?>

<div class="course-rel-student-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'course_id')->widget(Select2::className(),[
                'data' =>\app\modules\admin\models\Courses::getCoursesName(),
                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'pluginEvents'=>[
                    'change'=>"function(){
                            let id=$(this).val();
                            $.ajax({
                                url:url,
                                method:'POST',
                                data:{id:id},
                                success:function(response){
                                    let student=$('#courserelstudent-student_id');
                                    student.html('');
                                    if(response.status){
                                        response.item.map((item,index) => {
                                            let newOption=new Option(item.name,item.student_id);
                                            student.append(newOption);
                                        })    
                                    }
                                }
                            });
                        }"
                ]
            ]) ?>

            <?= $form->field($model, 'student_id')->widget(Select2::className(),[
                'data' => '',
                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

            <div class="card">
                <div class="card-body">
                    <p>
                        <?= Html::a(Yii::t('app', 'Create Student'), ['students/create-course-rel'],
                            ['class' => 'btn btn-success create-button']) ?>
                    </p>
                </div>
            </div>

        </div>
        <div class="col-lg-6">

            <?echo $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'kursning boshlanish sanasi ...'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>

            <?= $form->field($model, 'end_date')->widget(DatePicker::className(),[
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'options' => ['placeholder' => 'kursning tugash sanasi ...'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ],
                'pluginEvents'=>[
                    'change'=>'function(){
                            let id=$(this).val();
                            $.ajax({
                                url:url,
                                method:GET,
                                data:{id:id},
                                success:function(response){
                                    if(response.status){
                                        
                                    }
                                }
                            });
                        }'
                ]
            ]) ?>

            <?= $form->field($model, 'status')->dropDownList([
                    '1' => 'Active',
                    '0' => 'In_Active',
            ]) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
Modal::begin([

    'title' => Yii::t('app', 'Create Student'),
    'class' => 'modal',
    'size' => Modal::SIZE_EXTRA_LARGE,
]);

Modal::end();

$js = <<<JS
    $('body').delegate('.create-button','click',function (event){
        event.preventDefault();
        console.log($(this).attr('href'));
        $('.modal').modal('show').find('.modal-body').load($(this).attr('href'));
    })
    $('body').delegate('.save-button','click',function (event){
        event.preventDefault();
        let form = $(this).parents('form');
        let action = form.attr('action');
        console.log(form.serialize());
        $.ajax({
        url:action,
        type: 'POST',
        data: form.serialize(),
        success: function (response){
            
        }
        })
    })
JS;
$this->registerJs($js);

?>
