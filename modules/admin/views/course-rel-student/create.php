<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\CourseRelStudent */

$this->title = Yii::t('app', 'Create Course Rel Student');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Course Rel Students'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-rel-student-create">


    <div class="card">
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
