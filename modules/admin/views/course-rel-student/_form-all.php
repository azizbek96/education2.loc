<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\CourseRelStudent */
/* @var $form yii\widgets\ActiveForm */
/* @var $subject_level_id */
/* @var $student_waiting */

?>

<div class="course-rel-student-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'course_id')->widget(Select2::className(),[
                'data' =>\app\modules\admin\models\Courses::getCoursesName($model->subject_level_id),
                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

            <?= $form->field($model, 'status')->dropDownList([
                '1' => 'Active',
                '0' => 'In_Active',
            ]) ?>


        </div>
        <div class="col-lg-6">

            <?echo $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'kursning boshlanish sanasi ...'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>

            <?= $form->field($model, 'end_date')->widget(DatePicker::className(),[
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'options' => ['placeholder' => 'kursning tugash sanasi ...'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
        <div class="col-md-12">
            <table class="table table-grey">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col"><?= Yii::t('app','First_name');?></th>
                    <th scope="col"><?= Yii::t('app','Last_name');?></th>
                    <th scope="col"><?= Yii::t('app','Phone');?></th>
                    <th scope="col">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" checked id="checkAllInput">
                            <label class="form-check-label" for="flexCheckIndeterminate">
                                <?= Yii::t('app','Check All');?>
                            </label>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                    <?php $i=1; foreach($student_waiting as $key => $value):?>
                        <tr>
                            <td><?= $i; ?></td>
                            <td><?php echo $value['first_name']?></td>
                            <td><?php echo $value['last_name']?></td>
                            <td><?php echo $value['phone']?></td>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input check-all"
                                           name="CourseRelStudentAll[<?=$i;?>][check]"
                                           type="checkbox" value="true" checked
                                           id="courserelstudent-check-<?=$i;?>">
                                    <input type="hidden" value="<?= $value['student_id']?>" name="CourseRelStudentAll[<?=$i;?>][student]">
                                    <label class="form-check-label" for="courserelstudent-check-<?=$i;?>">
                                        check
                                    </label>
                                </div>
                            </td>
                        </tr>
                    <?php $i++; endforeach;?>
                </tbody>
            </table>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
    $js=<<<JS
        $("#checkAllInput").on('click',function() {
           if($(this)[0].checked){
               $('.check-all').prop("checked",true)
           }
           else{
               $('.check-all').prop("checked",false)
           }
        })
    JS;
    $this->registerJs($js);
?>
<?php
Modal::begin([

    'title' => Yii::t('app', 'Create Student'),
    'class' => 'modal',
    'size' => Modal::SIZE_EXTRA_LARGE,
]);

Modal::end();

$js = <<<JS
    $('body').delegate('.create-button','click',function (event){
        event.preventDefault();
        console.log($(this).attr('href'));
        $('.modal').modal('show').find('.modal-body').load($(this).attr('href'));
    })
    $('body').delegate('.save-button','click',function (event){
        event.preventDefault();
        let form = $(this).parents('form');
        let action = form.attr('action');
        console.log(form.serialize());
        $.ajax({
        url:action,
        type: 'POST',
        data: form.serialize(),
        success: function (response){
            
        }
        })
    })
JS;
$this->registerJs($js);

?>
