<?php

use app\modules\admin\models\CourseRelStudent;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\CourseRelStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Course Rel Students');
$this->params['breadcrumbs'][] = $this->title;
/***
 * @var $model
 */
$colours=[
        1=>'info',
        2=>'success',
        3=>'danger',
        4=>'warning'
];


?>
<div class="course-rel-student-index">
    <div class="card">
        <div class="card-body">
            <div class="row d-flex flex-wrap">
                <?php $i=1; foreach ($model as $key => $value): ?>
                    <div class="col-lg-3 col-6">

                        <div class="small-box bg-<?= $colours[$i]?>">
                            <div class="inner">
                                <h3><?php echo $value['count']?></h3>
                                <p><?php echo $value['name']?></p>
                            </div>
                            <a href="<?= Url::to(['create-all','subject_level_id'=>$value['id']])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                <?php
                    $i++;
                    if($i==4) $i=1;
                endforeach; ?>

            </div>
        </div>
    </div>
</div>
