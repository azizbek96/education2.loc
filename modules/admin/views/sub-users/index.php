<?php

/* @var $model \app\modules\admin\models\SubUsers */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title="Shaxsiy Ma'lumotlar";
?>
<div class="col-lg-12">
    <div class="card">
        <div class="card-body">
            <div class="userInfoUpdateButtons d-flex" style="flex-direction: row-reverse; letter-spacing: 1px; margin-bottom:20px ">
                <p>
                    <?= Html::a("<i class='fa fa-lock'></i> Parolni O'zgartirish",['change-password','id'=>$model->id],
                        [
                            'class' => 'btn btn-outline-danger',
                            'style'=>[
                                'display'=>'inline-block',
                            ],
                        ]) ?>
                </p>
                <p>
                    <?= Html::a('<i class="fa fa-save"> </i> Tahrirlash',['update','id'=>$model->id],
                        [
                            'class' => 'btn btn-outline-success btn-shadow-primary',
                            'style'=>[
                                'display'=>'inline-block',

                            ],
                        ]) ?>
                </p>

            </div>
            <?php
            echo DetailView::widget([
                'model'=>$model,
                'options'=>['class'=>'table table-responsive-sm '],
                'formatter'=>['class'=>'\yii\i18n\Formatter','nullDisplay'=>''],
                'attributes'=>[
                        'user.username',
                        'password',
                        [
                                'attribute' => 'image',
                                'value' => function($data)
                                {
                                    if(($data->image) != null)
                                    {
                                        return Html::img(Url::base().'/web/uploads/user/'.$data->image,[
                                                'width'=>'200px',
                                        ]);
                                    }
                                    return "<h6 class='text-danger'>no user photo</h6>";
                                },
                                'format' =>'raw'
                        ]
                ]
            ]);
            ?>
        </div>
    </div>
</div>