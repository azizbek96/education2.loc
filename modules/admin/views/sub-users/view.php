<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\SubUsers */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sub Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sub-users-view">

    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
//                    'id',
                    'user.username',
                    [
                        'attribute' => 'image',
                        'value' => function($data)
                        {
                            if(($data->image) != null)
                            {
                                return Html::img(Url::base().'/web/uploads/user/'.$data->image,[
                                    'width'=>'200px',
                                ]);
                            }
                            return "<h6 class='text-danger'>no user photo</h6>";
                        },
                        'format' =>'raw'
                    ]
                ],
            ]) ?>
        </div>
    </div>

</div>
