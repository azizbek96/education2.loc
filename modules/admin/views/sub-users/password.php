<?php
/* @var $model \app\modules\admin\models\Users */

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

$this->title="Update user password";
?>
<div class="user-info-update">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <?php $form = ActiveForm::begin(); ?>

                    <?php echo $form->field($model,'password')->textInput(['minlength'=>4])?>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 d-flex flex-row" style="letter-spacing: 1px">
                    <p>
                        <?= Html::submitButton('<i class="fa fa-save"> </i> Saqlash',
                            [
                                'class' => 'btn btn-outline-success btn-shadow-primary',
                                'style'=>[
                                    'margin-right'=>'10px'
                                ],
                            ]) ?>
                    </p>
                    <p>
                        <?= Html::a('Bekor qilish',['index'],
                            [
                                'class' => 'btn btn-outline-danger',
                                'style'=>[

                                ],
                            ]) ?>
                    </p>
                </div>
                <?php  ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
