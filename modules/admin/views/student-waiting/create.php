<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\StudentWaiting */

$this->title = Yii::t('app', 'Create Student Waiting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Student Waitings'), 'url' => ['index']];
?>
<div class="student-waiting-create">
    <div class="card">
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
