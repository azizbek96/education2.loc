<?php

use app\modules\admin\models\StudentWaiting;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\StudentWaitingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Student Waitings');
?>
<div class="student-waiting-index">
    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('app', 'Create Student Waiting'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout'=>'{items} {pager}',
                 'rowOptions' =>function($data)
                     {
                             if(!($data->status == 1))
                             {
                                 return ['class'=>'table-danger'];
                             }
                             return ['class' => 'table-success'];
                     },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' =>'student_id',
                        'value' =>function($model)
                        {
                            if(!($model->students->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->students->first_name.' '.$model->students->last_name."</button>";
                            }
                            return $model->students->first_name.' '.$model->students->last_name;
                        },
                        'format'=>'raw'
                    ],
                    'subject_level_id',
                    'wait_date',
                    [
                        'attribute'=>'status',
                        'value'=>function($data){
                            if($data->status == 0)
                            {
                                return "<button class='btn  btn-danger'>In_Active</button>";
                            }
                            return "<button class='btn  btn-primary'>Active</button>";
                        },
                        'format' => 'raw'
                    ],
                    'updated_by',
                    [
                        'attribute' => 'updated_at',
                        'value' => function($data){
                            // time utf formatda boladi 12212121 korinishida
                            $d1=new DateTime(date('Y-m-d H:i:s'));
                            $d2=new DateTime(date('Y-m-d H:i:s',$data->updated_at));
                            $diff=$d1->diff($d2);
                            return "<code>".$diff->format('%Y-%m-%d %H:%i:%s')."</code>";
                        },
                        'format'=>'raw',
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'buttons' => [
                            'view' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-eye"></span>',$url,[
                                    'class' => 'btn btn-xs btn-light view-button'
                                ]);
                            },
                            'update' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-pen"></span>',$url,[
                                    'class' => 'btn btn-xs btn-primary'
                                ]);
                            },
                            'delete' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-trash"></span>',$url,[
                                    'class' => 'btn btn-xs btn-danger',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' =>'Are you sure ?'
                                    ]
                                ]);
                            }
                        ],
                        'template' => '{update} {view} {delete}',
                        'contentOptions'=>['style' => 'width:120px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<?php
Modal::begin([

    'title' => Yii::t('app', 'Create Student'),
    'class' => 'modal',
    'size' => Modal::SIZE_EXTRA_LARGE,
]);

Modal::end();

$js = <<<JS
    $('body').delegate('.create-button,.view-button ,.update-button','click',function (event){
        event.preventDefault();
        console.log($(this).attr('href'));
        $('.modal').modal('show').find('.modal-body').load($(this).attr('href'));
    })
    $('body').delegate('.save-button','click',function (event){
        event.preventDefault();
        let form = $(this).parents('form');
        let action = form.attr('action');
        console.log(form.serialize());
        $.ajax({
        url:action,
        type: 'POST',
        data: form.serialize(),
        success: function (response){
            
        }
        })
    })
JS;
$this->registerJs($js);

?>
