<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\StudentWaiting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-waiting-form">
    
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'student_id')->widget(Select2::class,[
                'data' => \app\modules\admin\models\Students::getStudents(),
                'pluginOptions' => [
                        'placeholder'=>'studentni tanlang'
                ]
            ]) ?>

            <?= $form->field($model, 'status')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'subject_level_id')->widget(Select2::class,[
                'data' => \app\modules\admin\models\SubjectLevel::getSubjectName(),
                'pluginOptions' => [
                    'placeholder'=>'subjectni tanlang'
                ]
            ]) ?>

            <?= $form->field($model, 'wait_date')->widget(DatePicker::class,[
                'name' => 'check_date',
                'value' =>date('Y-mm-dd'),
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pickerIcon' => '<i class="fas fa-calendar-alt text-primary"></i>',
                'removeIcon' => '<i class="fas fa-trash text-danger"></i>',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
