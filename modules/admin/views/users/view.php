<?php

use app\modules\admin\models\AuthAssignment;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Users */

$this->title = 'Created user '.$model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
\yii\web\YiiAsset::register($this);
?>
<div class="users-view">
    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
//            'id',
                    'username',
                    'password',
                    [
                            'attribute' => 'auth_assigment',
                        'label' => Yii::t('app','Rule'),
                        'value' => function($model){
                            return AuthAssignment::getUserRule($model->id);
                        },
                        'format' => 'raw',
                    ],
                    'description',
                    [
                        'attribute'=>'created_at',
                        'value' => function($data)
                        {
                            return date('Y-m-d H:i:s', $data->created_at);
                        }
                    ],
                    [
                        'attribute'=>'updated_at',
                        'value' => function($data)
                        {
                            return date('Y-m-d H:i:s', $data->updated_at);
                        }
                    ],
                    'created_by',
                    'updated_by',
                ],
            ]) ?>
        </div>
    </div>


</div>
