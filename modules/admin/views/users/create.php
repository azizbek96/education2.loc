<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Users */

$this->title = 'Create Users';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-create">
   <div class="card">
       <div class="card-body">
           <?= $this->render('_form', [
               'model' => $model,
           ]) ?>
       </div>
   </div>
</div>
