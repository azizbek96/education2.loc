<?php

use app\modules\admin\models\AuthItem;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Users */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="users-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>


            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textarea(['rows'=>7,
                    'style' => ['resize' => 'none']
                ]) ?>

            <?= $form->field($model, 'auth_assigment')->widget(Select2::className(),[
                'data' => AuthItem::getListRule(),
                'options' => ['placeholder' => Yii::t('app','Tanlang...')],
            ])  ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

</div>
