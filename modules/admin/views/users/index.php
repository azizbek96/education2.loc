<?php

use app\modules\admin\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <?php
        \yii\bootstrap4\Modal::begin([
                'id' => 'users_view_modal'
        ]);
    ?>
    <div id="users_view_page"></div>
    <?php
        \yii\bootstrap4\Modal::end();
    ?>
    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => '{items} {pager}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'username',
                    'password',
                    [
                        'attribute'=>'created_at',
                        'value' => function($data)
                        {
//                            \yii\helpers\VarDumper::dump($data, 10,true);die();
                            return date('Y-m-d H:i:s', $data->created_at);
                        }
                    ],
                    'description',
                    [
                        'class' => ActionColumn::className(),
                        'buttons' => [
                               'view' => function($url,$model,$index){
                                     return Html::a('<span class="fa fa-eye"></span>',$url,[
                                             'class' => 'btn btn-xs btn-light',
                                             'id'=>'users_view'
                                     ]);
                               },
                               'update' => function($url,$model,$index){
                                    return Html::a('<span class="fa fa-pen"></span>',$url,[
                                            'class' => 'btn btn-xs btn-primary',
                                        'id' => 'users_update'
                                    ]);
                               },
                               'delete' => function($url,$model,$index){
                                    return Html::a('<span class="fa fa-trash"></span>',$url,[
                                        'class' => 'btn btn-xs btn-danger',
                                        'data' => [
                                                'method' => 'post',
                                                'confirm' =>'Are you sure ?'
                                        ]
                                    ]);
                               }
                        ],
                        'template' => '{update} {view} {delete}'
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<?php
$script = <<< JS
     $(document).ready(function (){
            $("#users_view,#users_update").on('click',function (e) {
            e.preventDefault();
            $("#users_view_page").load($(this).attr("href"));
            $("#users_view_modal").modal("show");
    })
     })
JS;
$this->registerJs($script);
?>