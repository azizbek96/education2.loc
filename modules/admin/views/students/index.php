<?php

use app\modules\admin\models\Students;
use yii\bootstrap4\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\StudentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Students');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="students-index">
    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('app', 'Create Students'), ['create'], ['class' => 'btn btn-success create-button',
                    'title' => Yii::t('app', 'Create Student')
                ]) ?>
            </p>


            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                 'layout'=>'{items} {pager}',
                 'rowOptions' =>function($data)
                     {
                             if(!($data->status == 1))
                             {
                                 return ['class'=>'table-danger'];
                             }
                             return ['class' => 'table-success'];
                     },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' =>'first_name',
                        'value'=>function($model)
                        {
                            return $model->first_name.' '.$model->last_name;
                        }
                    ],
                    'age',
                    [
                            'attribute'=>'phone',
                            'value'=>function($model)
                            {
                                return "<a href='tel:".$model->phone."'>$model->phone</a>";
                            },
                            'format'=>'raw',
                            'headerOptions' => [
                                    'style' => 'width:170px'
                            ]
                    ],
                    [
                        'attribute'=>'telegram_username',
                        'value'=>function($model)
                        {
                            return "<a href='https://telegram.me/".$model->telegram_username."' target='_blank'>$model->telegram_username</a>";
                        },
                        'format'=>'raw',
                        'headerOptions' => [
                            'style' => 'width:120px'
                        ]
                    ],
                    [
                        'attribute'=>'status',
                        'value'=>function($data){
                            if($data->status == 0)
                            {
                                return "<button class='btn  btn-danger'>In_Active</button>";
                            }
                            return "<button class='btn  btn-primary'>Active</button>";
                        },
                        'format' => 'raw'
                    ],
                    [
                        'attribute' => 'updated_at',
                        'value' => function($data){
                            // time utf formatda boladi 12212121 korinishida
                            $d1=new DateTime(date('Y-m-d H:i:s'));
                            $d2=new DateTime(date('Y-m-d H:i:s',$data->updated_at));
                            $diff=$d1->diff($d2);
                            return "<code>".$diff->format('%Y-%m-%d %H:%i:%s')."</code>";
                        },
                        'format'=>'raw',
                    ],
                    'updated_by',
                    [
                        'class' => ActionColumn::className(),
                        'buttons' => [
                            'view' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-eye"></span>',['view', 'id'=>$model->id],[
                                    'title'=>Yii::t('app','view'),
                                    'class' => 'btn btn-xs btn-info view-button'
                                ]);
                            },
                            'update' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-pen"></span>',['update', 'id'=>$model->id],[
                                    'title'=>Yii::t('app','update'),
                                    'class' => 'btn btn-xs btn-primary update-button'
                                ]);
                            },
                            'delete' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-trash"></span>',['delete', 'id'=>$model->id],[
                                    'title'=>Yii::t('app','delete'),
                                    'class' => 'btn btn-xs btn-danger',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' =>'Are you sure ?'
                                    ]
                                ]);
                            }
                        ],
                        'template' => '{update} {view} {delete}',
                        'contentOptions' => ['style' => 'width:110px']
                    ],
                ],
            ]); ?>
        </div>
    </div>


</div>

<?php
Modal::begin([

    'title' => Yii::t('app', 'Create Student'),
    'class' => 'modal',
    'size' => Modal::SIZE_EXTRA_LARGE,
]);

Modal::end();

$js = <<<JS
    $('body').delegate('.create-button,.view-button ,.update-button','click',function (event){
        event.preventDefault();
        console.log($(this).attr('href'));
        $('.modal').modal('show').find('.modal-body').load($(this).attr('href'));
    })
    $('body').delegate('.save-button','click',function (event){
        event.preventDefault();
        let form = $(this).parents('form');
        let action = form.attr('action');
        console.log(form.serialize());
        $.ajax({
        url:action,
        type: 'POST',
        data: form.serialize(),
        success: function (response){
            
        }
        })
    })
JS;
$this->registerJs($js);

?>