<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Students */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Students'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="students-view">

    <p>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'first_name',
            'last_name',
            'age',
            [
                'attribute'=>'phone',
                'value'=>function($model)
                {
                    return "<a href='tel:".$model->phone."'>$model->phone</a>";
                },
                'format'=>'raw'
            ],
            [
                    'attribute'=>'telegram_username',
                    'value'=>function($model)
                    {
                        return "<a href='https://telegram.me/".$model->telegram_username."'>$model->telegram_username</a>";
                    },
                    'format'=>'raw'
            ],
            [
                'attribute'=>'status',
                'value'=>function($data){
                    if($data->status == 0)
                    {
                        return "<button class='btn  btn-danger'>In_Active</button>";
                    }
                    return "<button class='btn  btn-primary'>Active</button>";
                },
                'format' => 'raw'
            ],
            [
                'attribute'=>'created_at',
                'value' => function($data)
                {
                    return date('Y-m-d H:i:s', $data->created_at);
                }
            ],
            [
                'attribute'=>'updated_at',
                'value' => function($data)
                {
                    return date('Y-m-d H:i:s', $data->updated_at);
                }
            ],
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
