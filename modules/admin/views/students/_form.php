<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Students */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="students-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'age')->textInput() ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'phone')->widget(MaskedInput::class,[
                    'mask' => '+\9\9\8(99) 999 99 99'
            ]) ?>

            <?= $form->field($model, 'telegram_username')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropDownList([
                '1' => 'Active',
                '0' => 'In_Active'
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'subject_level_id')->widget(\kartik\select2\Select2::class,[
                'data' =>\app\modules\admin\models\SubjectLevel::getSubjectName(),
                'pluginOptions'=>[
                        'placeholder' =>'Select a subject level'
                ]
            ]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'wait_date')->widget(\kartik\date\DatePicker::class) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
