<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\UserRelEmployee */

$this->title = Yii::t('app', 'Create User Rel Employee');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Rel Employees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-rel-employee-create">
    <div class="card">
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
