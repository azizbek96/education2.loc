<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\UserRelEmployee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-rel-employee-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->widget(\kartik\select2\Select2::class,[
            'data' =>\app\modules\admin\models\Users::getUserId(),
            'options'=>[
                    'placeholder'=>'select user...'
            ]
    ]) ?>

    <?= $form->field($model, 'employee_id')->widget(\kartik\select2\Select2::class,[
        'data' => \app\modules\admin\models\Employees::getEmployeeAll(),
        'options'=>[
            'placeholder'=>'select employee...'
        ]
    ]) ?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatus()) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
