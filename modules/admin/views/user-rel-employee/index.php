<?php

use app\modules\admin\models\UserRelEmployee;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UserRelEmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Rel Employees');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-rel-employee-index">
    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('app', 'Create User Rel Employee'), ['create'], ['class' => 'btn btn-success create-button']) ?>
            </p>

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => '{items} {pager}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    
                    [
                        'attribute' => 'user_id',
                        'value'=>'user.username'
                    ],
                    [
                        'attribute' =>'employee_id',
                        'value' =>function($model)
                        {
                            if(!($model->employee->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->employee->first_name.'<code>:'.$model->employee->position->name."</code></button>";
                            }
                            return $model->employee->first_name.' <code>:'.$model->employee->position->name.'</code>';
                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute'=>'status',
                        'value'=>function($data){
                            if($data->status == 0)
                            {
                                return "<button class='btn  btn-danger'>In_Active</button>";
                            }
                            return "<button class='btn  btn-primary'>Active</button>";
                        },
                        'format' => 'raw'
                    ],
                    [
                            'attribute'=>'updated_by',
                            'value'=>'user.username'
                    ],
                    [
                        'attribute' => 'updated_at',
                        'value' => function($data){
                            // time utf formatda boladi 12212121 korinishida
                            $d1=new DateTime(date('Y-m-d H:i:s'));
                            $d2=new DateTime(date('Y-m-d H:i:s',$data->updated_at));
                            $diff=$d1->diff($d2);
                            return "<code>".$diff->format('%Y-%m-%d %H:%i:%s')."</code>";
                        },
                        'format'=>'raw',
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'buttons' => [
                            'view' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-eye"></span>',$url,[
                                    'class' => 'btn btn-xs btn-light view-button'
                                ]);
                            },
                            'update' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-pen"></span>',$url,[
                                    'class' => 'btn btn-xs btn-primary update-button'
                                ]);
                            },
                            'delete' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-trash"></span>',$url,[
                                    'class' => 'btn btn-xs btn-danger',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' =>'Are you sure ?'
                                    ]
                                ]);
                            }
                        ],
                        'template' => '{update} {view} {delete}',
                        'headerOptions'=>[
                                'width'=>'120px'
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<?php
    Modal::begin([
            'title' => Yii::t('app','Create User Rel Employee'),
            'id'=>'ure_modal'
    ]);
    Modal::end();
?>
<?php
    $js=<<<JS
        $('body').delegate('.create-button,.view-button,.update-button','click',function(e){
            e.preventDefault();
            console.log($(this).attr('href'))
            $('#ure_modal').modal('show').find('.modal-body').load($(this).attr('href'));
        })
    JS;
    $this->registerJS($js)
?>