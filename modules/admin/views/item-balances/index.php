<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ItemBalancesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Item Balances');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-balances-index">
    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('app', 'Create Item Balances'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>


            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout'=>'{items} {pager}',
                 'rowOptions' =>function($data)
                     {
                             if(!($data->status == 1))
                             {
                                 return ['class'=>'table-danger'];
                             }
                             return ['class' => 'table-success'];
                     },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' =>'document_id',
                        'value' =>function($model)
                        {
                            if(!($model->document->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->document->id."</button>";
                            }
                            return $model->document->id;
                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute' =>'document_item_id',
                        'value' =>function($model)
                        {
                            if(!($model->documentItem->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->documentItem->name."</button>";
                            }
                            return $model->documentItem->type;
                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute' =>'employee_id',
                        'value' =>function($model)
                        {
                            if(!($model->employee->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->employee->name."</button>";
                            }
                            return $model->employee->first_name;
                        },
                        'format'=>'raw'
                    ],
                    'price',
                    'inventory',
                    [
                        'attribute' =>'course_id',
                        'value' =>function($model)
                        {
                            if(!($model->course->status == 1))
                            {
                                return "<button class='btn btn-danger'>".$model->course->name."</button>";
                            }
                            return $model->course->name;
                        },
                        'format'=>'raw'
                    ],
                    'reg_date',
                    [
                        'attribute'=>'status',
                        'value'=>function($data){
                            if($data->status == 0)
                            {
                                return "<button class='btn  btn-danger'>In_Active</button>";
                            }
                            return "<button class='btn  btn-primary'>Active</button>";
                        },
                        'format' => 'raw'
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'buttons' => [
                            'view' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-eye"></span>',$url,[
                                    'class' => 'btn btn-xs btn-light'
                                ]);
                            },
                            'update' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-pen"></span>',$url,[
                                    'class' => 'btn btn-xs btn-primary'
                                ]);
                            },
                            'delete' => function($url,$model,$index){
                                return Html::a('<span class="fa fa-trash"></span>',$url,[
                                    'class' => 'btn btn-xs btn-danger',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' =>'Are you sure ?'
                                    ]
                                ]);
                            }
                        ],
                        'template' => '{update} {view} {delete}',
                        'contentOptions' => [
                                'style' => 'width:120px'
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
