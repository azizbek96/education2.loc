<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\ItemBalances */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-balances-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'student_id')->widget(Select2::classname(), [
                'data' => \app\modules\admin\models\Students::getStudents(),
                'options' => ['placeholder' => 'Select a student ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'document_id')->widget(Select2::classname(), [
                'data' => \app\modules\admin\models\Documents::getDocuments(),
                'options' => ['placeholder' => 'Select a doc_id ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'document_item_id')->widget(Select2::classname(), [
                'data' => \app\modules\admin\models\DocumentItems::getDocumentItems(),
                'options' => ['placeholder' => 'Select a doc_item_id ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'employee_id')->widget(Select2::classname(), [
                'data' => \app\modules\admin\models\Employees::getEmployeesName(),
                'options' => ['placeholder' => 'Select a employee ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'price')->widget(\kartik\money\MaskMoney::class, [
                'name' => 'price',
                'value' => 1000,
                'pluginOptions' => [
                    'prefix' => ' ',
                    'suffix' => "  $",
                    'precision' => 0
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'course_id')->widget(Select2::classname(), [
                'data' => \app\modules\admin\models\Courses::getCoursesName(),
                'options' => ['placeholder' => 'Select a course ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'inventory')->widget(\kartik\money\MaskMoney::class, [
                'name' => 'inventory',
                'value' => 1000,
                'pluginOptions' => [
                    'prefix' => ' ',
                    'suffix' => "  $",
                    'precision' => 0
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'reg_date')->widget(DatePicker::classname(), [
                'name' => 'reg_date',
                'value'=>date('Y-m-d'),
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pickerIcon' => '<i class="fas fa-calendar-alt text-primary"></i>',
                'removeIcon' => '<i class="fas fa-trash text-danger"></i>',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model->getStatus()) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
