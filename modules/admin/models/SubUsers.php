<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sub_users".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $image
 * @property string|null $password
 * @property string|null $username
 *
 * @property Users $user
 */
class SubUsers extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sub_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['image','password','username'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'image' => 'Image',
        ];
    }
    public static function getUsersName()
    {
        $employee=Employees::find()
            ->alias('e')
            ->select([
                'e.firstname',
                'e.lastname',
                'e.age',
                'e.phone',
                'e.photo',
                'e.email',
                'p.name',
            ])
            ->leftJoin(['ure' => 'user_rel_employee'],'ure.employee.id=e.id')
            ->leftJoin(['u'=>'users'],'u.id=ure.user.id')
            ->leftJoin(['p' => 'position'],'p.id=e.position_id')
            ->where(['u.id'=>\Yii::$app->user->id])
            ->asArray()->one();
        return $employee;
    }
    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
