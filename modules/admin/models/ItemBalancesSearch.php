<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\ItemBalances;

/**
 * ItemBalancesSearch represents the model behind the search form of `app\modules\admin\models\ItemBalances`.
 */
class ItemBalancesSearch extends ItemBalances
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'student_id', 'document_id', 'document_item_id', 'employee_id', 'course_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['price', 'inventory'], 'number'],
            [['reg_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ItemBalances::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'student_id' => $this->student_id,
            'document_id' => $this->document_id,
            'document_item_id' => $this->document_item_id,
            'employee_id' => $this->employee_id,
            'price' => $this->price,
            'inventory' => $this->inventory,
            'course_id' => $this->course_id,
            'reg_date' => $this->reg_date,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
