<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "documents".
 *
 * @property int $id
 * @property int|null $course_id
 * @property int|null $employee_id
 * @property string|null $reg_date
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $updated_at
 * @property int|null $created_at
 * @property string|null $document_number
 * @property int|null $document_type
 * @property string|null $add_info
 *
 * @property Courses $course
 * @property Employees $employee
 * @property DocumentItems $documentItems
 */
class Documents extends \yii\db\ActiveRecord
{
    const DOC_TYPE_INCOMING=1;
    const DOC_TYPE_OUTGOING=2;
    const DOC_TYPE_INCOMING_LABEL='incoming';
    const DOC_TYPE_OUTGOING_LABEL='outgoing';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'documents';
    }
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
            ],
            [
                'class' => TimestampBehavior::class,
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id', 'employee_id', 'status', 'created_by', 'updated_by', 'updated_at','created_at', 'document_type'], 'integer'],
            [['reg_date'], 'safe'],
            [['add_info'], 'string'],
            [['document_number'], 'string', 'max' => 50],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employees::className(), 'targetAttribute' => ['employee_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'course_id' => Yii::t('app', 'Course ID'),
            'employee_id' => Yii::t('app', 'Employee ID'),
            'reg_date' => Yii::t('app', 'Reg Date'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'document_number' => Yii::t('app', 'Doc Number'),
            'document_type' => Yii::t('app', 'Doc Type'),
            'add_info' => Yii::t('app', 'Add Info'),
        ];
    }

    /**
     * Gets query for [[Course]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['id' => 'course_id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employees::className(), ['id' => 'employee_id']);
    }
    public function getDocumentItems()
    {
        return $this->hasMany(DocumentItems::className(), ['doc_id' => 'id']);
    }
    public static function getDocTypeBySlug($key = null)
    {
        $list=[
            self::DOC_TYPE_INCOMING_LABEL=>Yii::t('app','Incoming'),
            self::DOC_TYPE_OUTGOING_LABEL=>Yii::t('app','Outgoing'),
        ];
        if($key != null)
        {
            return $list[$key];
        }
        return $list;
    }

    public static function getLastDocType($doc_type = null)
    {
        $type = Documents::find()->filterWhere(['document_type'=>$doc_type])
            ->orderBy(['id'=>SORT_DESC])->asArray()->all();

        return !empty($type)?count($type):1;
    }

}
