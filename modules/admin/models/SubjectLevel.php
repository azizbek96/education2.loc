<?php

namespace app\modules\admin\models;

use Yii;
use app\components\OurCustomBehavior\OurCustomBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "subject_level".
 *
 * @property int $id
 * @property int $subject_id
 * @property string|null $level_name
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Subject $subject
 */
class SubjectLevel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subject_level';
    }
    /**
     *
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => OurCustomBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by', 'updated_by'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject_id'], 'required'],
            [['subject_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['level_name'], 'string', 'max' => 50],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subject_id' => Yii::t('app', 'Subject ID'),
            'level_name' => Yii::t('app', 'Level Name'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Courses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Courses::className(), ['subject_level_id' => 'id']);
    }

    /**
     * Gets query for [[StudentWaitings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentWaitings()
    {
        return $this->hasMany(StudentWaiting::className(), ['subject_level_id' => 'id']);
    }

    /**
     * Gets query for [[Subject]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    public static function getSubjectName()
    {
        $data = SubjectLevel::find()
            ->alias('sl')
            ->select([
                'sl.id AS id',
                "CONCAT(s.name,'(',sl.level_name,')') AS fullname"
            ])->leftJoin(['s'=>'subject'],'s.id=sl.subject_id')
            ->where(['s.status'=>1])
            ->andWhere(['sl.status'=>1])->asArray()->all();
        return ArrayHelper::map($data,'id','fullname');
    }
    public static function getSubjectLevelId($id)
    {
        $data=self::find()
            ->alias('sl')
            ->select([
                'sl.id AS id',
                "CONCAT(s.name,'(',sl.level_name,')') AS name"
            ])
            ->leftJoin(['s'=>'subject'],'s.id=sl.subject_id')
            ->where(['s.status'=>1])
            ->andWhere(['sl.subject_id'=>$id])
            ->andWhere(['sl.status'=>1])
            ->asArray()->all();
        return $data;
    }
}
