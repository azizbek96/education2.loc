<?php

namespace app\modules\admin\models;

use app\components\OurCustomBehavior\OurCustomBehavior;
use Imagine\Image\ImageInterface;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\imageAttachment\ImageAttachmentBehavior;

/**
 * This is the model class for table "employees".
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property int|null $age
 * @property string|null $phone
 * @property string|null $photo
 * @property string|null $email
 * @property string|null $file
 * @property int $position_id
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property Attendance[] $attendances
 * @property Courses[] $courses
 * @property Documents[] $documents
 * @property EmployeeRelSubject[] $employeeRelSubjects
 * @property ItemBalances[] $itemBalances
 * @property Position $position
 * @property Users $updatedBy
 */
class Employees extends \yii\db\ActiveRecord
{
    public $imageFile;
    public $exeFile;
    public $pos_name;
    public $pos_description;
    const ACTIVE=1;
    const IN_ACTIVE=0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees';
    }
    /***
     *
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => OurCustomBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by', 'updated_by'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['age', 'position_id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['imageFile'],'file',],
            [['exeFile'],'file',],
            [['first_name', 'last_name', 'phone', 'email'], 'string', 'max' => 50],
            [['photo', 'file'], 'string', 'max' => 120],
            [['position_id'], 'exist', 'skipOnError' => true, 'targetClass' => Position::className(), 'targetAttribute' => ['position_id' => 'id']],
            [['pos_name','pos_description'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'age' => Yii::t('app', 'Age'),
            'phone' => Yii::t('app', 'Phone'),
            'photo' => Yii::t('app', 'Photo'),
            'email' => Yii::t('app', 'Email'),
            'file' => Yii::t('app', 'File'),
            'position_id' => Yii::t('app', 'Position ID'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    public function uploadImg()
    {
        if ($this->validate()) {
            $date=date('YmdHis');
            if(!empty($this->imageFile->saveAs('uploads/image/'.$date.'.'.$this->imageFile->extension))) {
                $this->photo = $date . '.' . $this->imageFile->extension;
                return true;
            }
        } else {
            return false;
        }
    }
    public function uploadFile()
    {
        if ($this->validate()) {
            $date=date('YmdHis');
            if(!empty($this->exeFile->saveAs('uploads/file/'.$date.'.'.$this->exeFile->extension))) {
                $this->file = $date . '.' . $this->exeFile->extension;
                return true;
            }
        } else {
            return false;
        }
    }
    /**
     * Gets query for [[Attendances]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAttendances()
    {
        return $this->hasMany(Attendance::className(), ['employee_id' => 'id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Courses::className(), ['employee_id' => 'id']);
    }

    /**
     * Gets query for [[Documents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Documents::className(), ['employee_id' => 'id']);
    }

    /**
     * Gets query for [[EmployeeRelSubjects]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeRelSubjects()
    {
        return $this->hasMany(EmployeeRelSubject::className(), ['employee_id' => 'id']);
    }

    /**
     * Gets query for [[ItemBalances]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getItemBalances()
    {
        return $this->hasMany(ItemBalances::className(), ['employee_id' => 'id']);
    }

    /**
     * Gets query for [[Position]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['id' => 'position_id']);
    }
    public function getPosName()
    {
        return $this->hasMany(Position::className(), ['position_id' => 'id']);
    }
    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserRelEmployees()
    {
        return $this->hasMany(UserRelEmployee::className(), ['employee_id' => 'id']);
    }
    public function getPositionId()
    {
        $data=ArrayHelper::map(Position::find()->all(),'id','name');
        return $data;
    }
    public function getStatus()
    {
        return [
            self::IN_ACTIVE,
            self::ACTIVE,
        ];
    }

    public static function getEmployeeAll()
    {
        $employee = Employees::find()
            ->alias('e')
            ->select([
                'e.id',
                "CONCAT(e.first_name,'<code>: ',p.name,'</code>') AS fullName"
            ])
            ->leftJoin(['p'=>'position'],'p.id=e.position_id')
            ->where(['p.status' => 1])
            ->andWhere(['e.status'=>1])
            ->asArray()->all();
        $array=ArrayHelper::map($employee,'id','fullName');
        return $array;
    }
    public static function getEmployeeList()
    {
        $employee = Employees::find()
            ->alias('e')
            ->select([
                'e.id',
                "CONCAT(e.first_name,' ',e.last_name,' (',p.name,')') AS fullName"
            ])
            ->leftJoin(['p'=>'position'],'p.id=e.position_id')
            ->where(['p.status' => 1])
            ->andWhere(['e.status'=>1])
            ->asArray()->all();
        $array=ArrayHelper::map($employee,'id','fullName');
        return $array;
    }
}
