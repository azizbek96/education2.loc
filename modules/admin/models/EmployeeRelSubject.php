<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "employee_rel_subject".
 *
 * @property int $id
 * @property int $subject_id
 * @property int $employee_id
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $description
 * @property int|null $sub_name;
 * @property int|null $sub_level;
 *
 * @property Employees $employee
 * @property Subject $subject
 */
class EmployeeRelSubject extends \yii\db\ActiveRecord
{
    public $sub_name;
    public $sub_level;
    const ACTIVE=1;
    const IN_ACTIVE=0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_rel_subject';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class,
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject_id', 'employee_id'], 'required'],
            [['subject_id', 'employee_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employees::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_id' => 'id']],
            [['description','sub_name','sub_level'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subject_id' => Yii::t('app', 'Subject ID'),
            'employee_id' => Yii::t('app', 'Employee ID'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employees::className(), ['id' => 'employee_id']);
    }

    /**
     * Gets query for [[Subject]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }
    public function getStatus()
    {
        return [
            0 => 'In_Active',
            1 => 'Active'
        ];
    }
    public function getList()
    {
        $list=Employees::find()->where(['status' =>self::ACTIVE])->asArray()->all();
        return ArrayHelper::map($list,'id','first_name');
    }
}
