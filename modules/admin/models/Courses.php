<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use function React\Promise\all;

/**
 * This is the model class for table "courses".
 *
 * @property int $id
 * @property string|null $name
 * @property int $subject_id
 * @property int $employee_id
 * @property int $subject_level_id
 * @property string|null $start_date
 * @property string|null $end_date
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $course_date;
 *
 * @property Attendance[] $attendances
 * @property CourseDate[] $courseDates
 * @property CourseRelStudent[] $courseRelStudents
 * @property Documents[] $documents
 * @property Employees $employee
 * @property ItemBalances[] $itemBalances
 * @property Subject $subject
 * @property SubjectLevel $subjectLevel
 */
class Courses extends \yii\db\ActiveRecord
{
    const IN_ACTIVE = 0;
    const ACTIVE = 1;
    public $course_date;
    const WEEK_DU=1;
    const WEEK_SE=2;
    const WEEK_CHO=3;
    const WEEK_PA=4;
    const WEEK_JU=5;
    const WEEK_SHA=6;
    const WEEK_YA=7;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'courses';
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class,
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject_id', 'employee_id', 'subject_level_id'], 'required'],
            [['subject_id', 'employee_id', 'subject_level_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['start_date', 'end_date'], 'date','format'=>'yyyy-mm-dd'],
            [['name'], 'string', 'max' => 50],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employees::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_id' => 'id']],
            [['subject_level_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubjectLevel::className(), 'targetAttribute' => ['subject_level_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'subject_id' => 'Subject ID',
            'employee_id' => 'Employee ID',
            'subject_level_id' => 'Subject Level ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'status' => 'Status',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Attendances]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAttendances()
    {
        return $this->hasMany(Attendance::className(), ['course_id' => 'id']);
    }

    /**
     * Gets query for [[CourseDates]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourseDates()
    {
        return $this->hasMany(CourseDate::className(), ['course_id' => 'id']);
    }

    /**
     * Gets query for [[CourseRelStudents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourseRelStudents()
    {
        return $this->hasMany(CourseRelStudent::className(), ['course_id' => 'id']);
    }

    /**
     * Gets query for [[Documents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Documents::className(), ['course_id' => 'id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employees::className(), ['id' => 'employee_id']);
    }

    /**
     * Gets query for [[ItemBalances]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getItemBalances()
    {
        return $this->hasMany(ItemBalances::className(), ['course_id' => 'id']);
    }

    /**
     * Gets query for [[Subject]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    /**
     * Gets query for [[SubjectLevel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectLevel()
    {
        return $this->hasOne(SubjectLevel::className(), ['id' => 'subject_level_id']);
    }

    public function getStatus()
    {
        return [
            self::IN_ACTIVE,
            self::ACTIVE,
        ];
    }


    public function getEmployeeId()
    {
        $data = ArrayHelper::map(Employees::find()->all(), 'id', 'first_name');
        return $data;
    }

    public function getSubjectLevelId()
    {
        $data = ArrayHelper::map(SubjectLevel::find()->all(), 'id', 'level_name');
        return $data;
    }
    public static function getCoursesName($subject_level_id = null)
    {
        $courses=self::find()
            ->alias('c')
            ->select([
                'c.id',
                "CONCAT(c.name,'(',sl.level_name,')') AS levelName",
            ])
            ->leftJoin(['s'=>'subject'],'s.id=c.subject_id')
            ->leftJoin(['e'=>'employees'],'e.id=c.employee_id')
            ->leftJoin(['sl'=>'subject_level'],'sl.id=c.subject_level_id')
            ->where(['s.status'=>1])
            ->andWhere(['e.status'=>1])
            ->filterWhere(['c.subject_level_id'=>$subject_level_id])
            ->andWhere(['sl.status'=>1])
            ->andWhere(['c.status'=>1])->asArray()->all();
        return ArrayHelper::map($courses,'id','levelName');
    }

    public static function getWeeks($key = null)
    {
        $list=[
            self::WEEK_DU => 'Dushanba',
            self::WEEK_SE => 'Seshanba',
            self::WEEK_CHO => 'CHorshanba',
            self::WEEK_PA => 'Payshanba',
            self::WEEK_JU => 'Juma',
            self::WEEK_SHA => 'Shanba',
            self::WEEK_YA => 'Yakshanba',
        ];
        if($key !=null)
        {
            return $list[$key];
        }
        return $list;
    }

    public static function getCoursesNameByEmployeeId($id)
    {
        $course = self::find()->alias('c')
            ->select([
                'c.id as id',
                "CONCAT(c.name,' (',sl.level_name,') ') as name"
            ])
            ->leftJoin(['s'=>'subject'],'s.id=c.subject_id')
            ->leftJoin(['e'=>'employees'],'e.id=c.employee_id')
            ->leftJoin(['sl'=>'subject_level'],'sl.id=c.subject_level_id')
            ->filterWhere(['c.employee_id'=>$id])
            ->where(['s.status'=>1])
            ->andWhere(['e.status'=>1])
            ->andWhere(['sl.status'=>1])
            ->andWhere(['c.status'=>1])->asArray()->all();
        return $course;
    }
}
