<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "document_items".
 *
 * @property int $id
 * @property int|null $student_id
 * @property float|null $price
 * @property string|null $price_date
 * @property int|null $type
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $add_info
 * @property int|null $doc_id
 *
 * @property Documents $doc
 * @property Students $student
 */
class DocumentItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document_items';
    }
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
            ],
            [
                'class' => TimestampBehavior::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_id', 'type', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at', 'doc_id'], 'integer'],
            [['price'], 'number'],
            [['price_date'], 'safe'],
            [['add_info'], 'string'],
            [['doc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Documents::className(), 'targetAttribute' => ['doc_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Students::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student_id' => Yii::t('app', 'Student ID'),
            'price' => Yii::t('app', 'Price'),
            'price_date' => Yii::t('app', 'Price Date'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'add_info' => Yii::t('app', 'Add Info'),
            'doc_id' => Yii::t('app', 'Doc ID'),
        ];
    }
    public function beforeSave($insert)
    {
        if($insert)
        {
            $this->price_date=($this->price_date)?date('Y-m-d',strtotime($this->price_date)):date('Y-m-d');
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
    public function afterFind()
    {
        $this->price_date=($this->price_date)?date('d.m.Y',strtotime($this->price_date)):date('d.m.Y');
        parent::afterFind();
    }

    /**
     * Gets query for [[Doc]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoc()
    {
        return $this->hasOne(Documents::className(), ['id' => 'doc_id']);
    }

    /**
     * Gets query for [[Student]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Students::className(), ['id' => 'student_id']);
    }
}
