<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "student_waiting".
 *
 * @property int $id
 * @property int|null $student_id
 * @property int|null $subject_level_id
 * @property string|null $wait_date
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class StudentWaiting extends \yii\db\ActiveRecord
{
    public $level_id=null;
    public $course_id=null;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_waiting';
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class,
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_id', 'subject_level_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['wait_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student_id' => Yii::t('app', 'Student ID'),
            'subject_level_id' => Yii::t('app', 'Subject Level ID'),
            'wait_date' => Yii::t('app', 'Wait Date'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    public function getStudents()
    {
        return $this->hasOne(Students::class,['id'=>'student_id']);
    }
    public function getSubjectLevel()
    {
        return $this->hasOne(SubjectLevel::class,['id'=>'subject_level_id']);
    }
    public static function getStudentWaitingAll($level_id=null,$course_id=null)
    {
        if(empty($level_id) && empty($course_id)) return [];
        $data=self::find()
            ->alias('sw')
            ->select([
                'st.first_name',
                'st.last_name',
                'st.phone',
                "CONCAT(st.first_name,' (',st.last_name,') ') AS name",
                'st.id AS student_id',
            ])
            ->leftJoin(['c'=>'courses'],'c.subject_level_id=sw.subject_level_id')
            ->leftJoin(['st'=>'students'],'st.id=sw.student_id')
            ->leftJoin(['sl'=>'subject_level'],'sl.id=sw.subject_level_id')
            ->leftJoin(['sb'=>'subject'],'sb.id=sl.subject_id');
            if(($level_id) !=null || ($course_id)!=null)
            {
                $data=$data->filterWhere(['c.id'=>$course_id])
                    ->andFilterWhere(['sw.subject_level_id'=>$level_id]);
            }
            $data=$data->andWhere(['sw.status'=>1])->asArray()->all();
        return $data;
    }

    public static function getStudentByCourse($course_id)
    {
        $list=self::find()->alias('st')
            ->select([
                'st.student_id as student_id',
                "CONCAT(s.first_name,' ',s.last_name) as fullname"
            ])
            ->leftJoin(['s'=>'students'],'s.id=st.student_id')
            ->where([
                'subject_level_id'=>(Courses::find()->select(['subject_level_id'])->where(['id'=>$course_id])->one())
            ])->asArray()->all();
        return $list;
    }
}
