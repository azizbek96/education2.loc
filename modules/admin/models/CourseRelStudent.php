<?php

namespace app\modules\admin\models;

use app\components\OurCustomBehavior\OurCustomBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "course_rel_student".
 *
 * @property int $id
 * @property int $course_id
 * @property int $student_id
 * @property string|null $start_date
 * @property string|null $end_date
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Courses $course
 * @property Students $student
 */
class CourseRelStudent extends \yii\db\ActiveRecord
{
    public $subject_level_id=null;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_rel_student';
    }
    /**
     *
     */

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => OurCustomBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by', 'updated_by'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id', 'student_id'], 'required'],
            [['course_id', 'student_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Students::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'student_id' => 'Student ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'status' => 'Status',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Course]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['id' => 'course_id']);
    }

    /**
     * Gets query for [[Student]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Students::className(), ['id' => 'student_id']);
    }

    public function getStudentId()
    {
        $data = ArrayHelper::map(Students::find()->all(), 'id', 'first_name');
        return $data;
    }
    public static function getStudentWaitingSubjectList()
    {
        $list=Subject::find()->alias('s')
            ->select([
                'sl.id',
                "CONCAT(s.name,' <code>',sl.level_name,'</code>') AS name",
                'COUNT(sw.id) AS count'
            ])
            ->leftJoin(['sl'=>'subject_level'],'s.id=sl.subject_id')
            ->leftJoin(['sw'=>'student_waiting'],'sw.subject_level_id=sl.id')
            ->where(['sw.status'=>1])
            ->groupBy(['sw.subject_level_id'])
            ->asArray()->all();
        return $list;
    }
    public function getSubjectLevelId()
    {
        $val =self::find()
            ->alias('c')
            ->select([
                'c.id AS id',
                "CONCAT(c.name,' (',sl.level_name,') ') AS level_name"])
            ->leftJoin(['sl'=>'subject_level'],'c.subject_level_id=sl.id')
            ->where(['sl.status'=>1])
//            ->filterWhere(['c.id'=>$course_id])
            ->asArray()->all();
        $data=ArrayHelper::map($val,'id','level_name');
        return $data;
    }

    public static function getCourseRelStudent()
    {
        $list=self::find()->alias('crs')
            ->select([
                's.id as id',
                "CONCAT(c.name,':',s.last_name,' ',s.first_name) as name"
            ])
            ->leftJoin(['c'=>'courses'],'c.id=crs.course_id')
            ->leftJoin(['s'=>'students'],'s.id=crs.student_id')
            ->asArray()->all();
        return ArrayHelper::map($list,'id','name');
    }
}
