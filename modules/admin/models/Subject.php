<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "subject".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $level
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Courses[] $courses
 * @property EmployeeRelSubject[] $employeeRelSubjects
 * @property StudentWaiting[] $studentWaitings
 * @property SubjectLevel[] $subjectLevels
 */
class Subject extends \yii\db\ActiveRecord
{
    const ACTIVE=1;
    const IN_ACTIVE=0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subject';
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class,
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['name', 'level'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'level' => 'Level',
            'status' => 'Status',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Courses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Courses::className(), ['subject_id' => 'id']);
    }

    /**
     * Gets query for [[EmployeeRelSubjects]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeRelSubjects()
    {
        return $this->hasMany(EmployeeRelSubject::className(), ['subject_id' => 'id']);
    }

    /**
     * Gets query for [[StudentWaitings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentWaitings()
    {
        return $this->hasMany(StudentWaiting::className(), ['subject_id' => 'id']);
    }

    /**
     * Gets query for [[SubjectLevels]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectLevels()
    {
        return $this->hasMany(SubjectLevel::className(), ['subject_id' => 'id']);
    }
    public function getStatus()
    {
        return [
            self::IN_ACTIVE,
            self::ACTIVE,
        ];
    }
    public static function getSubjects()
    {
        $subject=Subject::find()
            ->select([
                'id',
                "CONCAT(name,'<code>:',level,'</code>') AS levelName"
            ])
            ->asArray()
            ->andWhere(['status'=>1])->all();
        return ArrayHelper::map($subject,'id','levelName');
    }

}
