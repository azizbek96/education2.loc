<?php

namespace app\modules\admin\models;

use app\modules\admin\models\Documents;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * DocumentsSearch represents the model behind the search form of `app\modules\admin\models\Documents`.
 */
class DocumentsSearch extends Documents
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'employee_id', 'status', 'created_by', 'updated_by', 'updated_at', 'document_type'], 'integer'],
            [['reg_date', 'document_number', 'add_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Documents::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'course_id' => $this->course_id,
            'employee_id' => $this->employee_id,
            'reg_date' => $this->reg_date,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'document_type' => $this->document_type,
        ]);

        $query->andFilterWhere(['like', 'document_number', $this->document_number])
            ->andFilterWhere(['like', 'add_info', $this->add_info]);

        return $dataProvider;
    }
    public function search_($id)
    {
        $query = DocumentItems::find()->where(['doc_id'=>$id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
