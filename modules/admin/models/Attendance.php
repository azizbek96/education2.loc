<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "attendance".
 *
 * @property int $id
 * @property int $course_id
 * @property int $employee_id
 * @property string|null $attendance_date
 * @property int|null $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property AttendanceItem[] $attendanceItems
 * @property Courses $course
 * @property Employees $employee
 */
class Attendance extends \yii\db\ActiveRecord
{
    const ACTIVE=1;
    const IN_ACTIVE=0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attendance';
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class,
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id', 'employee_id'], 'required'],
            [['course_id', 'employee_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['attendance_date'], 'safe'],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employees::className(), 'targetAttribute' => ['employee_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'employee_id' => 'Employee ID',
            'attendance_date' => 'Attendance Date',
            'status' => 'Status',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[AttendanceItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAttendanceItems()
    {
        return $this->hasMany(AttendanceItem::className(), ['attendance_id' => 'id']);
    }

    /**
     * Gets query for [[Course]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['id' => 'course_id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employees::className(), ['id' => 'employee_id']);
    }

    public function getStatus()
    {
        return [
            self::IN_ACTIVE,
            self::ACTIVE,
        ];
    }
    public static function getAttendaceName()
    {
        $attendance = self::find()->where(['status' => 1])->all();
        return ArrayHelper::map($attendance,'id','id');
    }
}
