<?php

namespace app\modules\admin\models;

use app\components\OurCustomBehavior\OurCustomBehavior;
use Imagine\Image\ImageInterface;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\imageAttachment\ImageAttachmentBehavior;

/**
 * This is the model class for table "students".
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property int|null $age
 * @property string|null $phone
 * @property string|null $telegram_username
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $subject_level_id
 * @property int|null $wait_date
 *
 * @property AttendanceItem[] $attendanceItems
 * @property CourseRelStudent[] $courseRelStudents
 * @property DocumentItems[] $documentItems
 * @property ItemBalances[] $itemBalances
 */
class Students extends \yii\db\ActiveRecord
{
    const ACTIVE=1;
    const IN_ACTIVE=0;
    public $subject_level_id;
    public $wait_date;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'students';
    }

    /***
     *
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['age', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['first_name', 'last_name', 'phone', 'telegram_username'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'age' => 'Age',
            'phone' => 'Phone',
            'telegram_username' => 'Username',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[AttendanceItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAttendanceItems()
    {
        return $this->hasMany(AttendanceItem::className(), ['student_id' => 'id']);
    }

    /**
     * Gets query for [[CourseRelStudents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourseRelStudents()
    {
        return $this->hasMany(CourseRelStudent::className(), ['student_id' => 'id']);
    }

    /**
     * Gets query for [[DocumentItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentItems()
    {
        return $this->hasMany(DocumentItems::className(), ['student_id' => 'id']);
    }

    /**
     * Gets query for [[ItemBalances]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getItemBalances()
    {
        return $this->hasMany(ItemBalances::className(), ['student_id' => 'id']);
    }

    public function getStatus()
    {
        return [
            self::IN_ACTIVE,
            self::ACTIVE,
        ];
    }

    public static function getStudents()
    {
        $students=self::find()->where(['status' => 1])->all();
        return ArrayHelper::map($students,'id','first_name');
    }
}
