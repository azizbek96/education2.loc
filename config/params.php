<?php

return [
    'bsVersion'=>'4.x',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'adminEmail' => 'admin@example.com',
    'language' => [
        'uz' => 'UZ',
        'ru' => 'RU',
        'en' => 'EN'
    ],
    'bsDependencyEnabled' => false,
    'hail812/yii2-adminlte3' => [
        'pluginMap' => [
            'sweetalert2' => [
                'css' => 'sweetalert2-theme-bootstrap-4/bootstrap-4.min.css',
                'js' => 'sweetalert2/sweetalert2.min.js'
            ],
            'toastr' => [
                'css' => ['toastr/toastr.min.css'],
                'js' => ['toastr/toastr.min.js']
            ],
        ]
    ],
    'maskMoneyOptions' => [
        'prefix' => 'US$ ',
        'suffix' => '',
        'affixesStay' => true,
        'thousands' => ',',
        'decimal' => '.',
        'precision' => 2,
        'allowZero' => false,
        'allowNegative' => false,
    ]
];
