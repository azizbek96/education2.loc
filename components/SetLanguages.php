<?php
namespace app\components;


use Yii;

class SetLanguages extends \yii\base\Behavior
{
    public function events()
    {
        return [
            \yii\base\Application::EVENT_BEFORE_REQUEST => 'func'
        ];
    }

    public function func(){
        if (Yii::$app->session->has('language')){
            Yii::$app->language = Yii::$app->session->get('language');
        }
    }
}