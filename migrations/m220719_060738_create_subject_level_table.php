<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%subject_level}}`.
 */
class m220719_060738_create_subject_level_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%subject_level}}', [
            'id' => $this->primaryKey(),
            'subject_id' => $this->integer()->notNull(),
            'level_name' => $this->string(50),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `subject_id`
        $this->createIndex(
            '{{%idx-subject_level-subject_id}}',
            '{{%subject_level}}',
            'subject_id'
        );

        // add foreign key for table `{{%subject}}`
        $this->addForeignKey(
            '{{%fk-subject_level-subject_id}}',
            '{{%subject_level}}',
            'subject_id',
            '{{%subject}}',
            'id',
            'RESTRICT'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%subject}}`
        $this->dropForeignKey(
            '{{%fk-subject_level-subject_id}}',
            '{{%subject_level}}'
        );

        // drops index for column `subject_id`
        $this->dropIndex(
            '{{%idx-subject_level-subject_id}}',
            '{{%subject_level}}'
        );

        $this->dropTable('{{%subject_level}}');
    }
}
