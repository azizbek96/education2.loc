<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employee_rel_subject}}`.
 */
class m220719_062302_create_employee_rel_subject_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employee_rel_subject}}', [
            'id' => $this->primaryKey(),
            'subject_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `subject_id`
        $this->createIndex(
            '{{%idx-employee_rel_subject-subject_id}}',
            '{{%employee_rel_subject}}',
            'subject_id'
        );

        // add foreign key for table `{{%subject}}`
        $this->addForeignKey(
            '{{%fk-employee_rel_subject-subject_id}}',
            '{{%employee_rel_subject}}',
            'subject_id',
            '{{%subject}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `employee_id`
        $this->createIndex(
            '{{%idx-employee_rel_subject-employee_id}}',
            '{{%employee_rel_subject}}',
            'employee_id'
        );

        // add foreign key for table `{{%employees}}`
        $this->addForeignKey(
            '{{%fk-employee_rel_subject-employee_id}}',
            '{{%employee_rel_subject}}',
            'employee_id',
            '{{%employees}}',
            'id',
            'RESTRICT'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%subject}}`
        $this->dropForeignKey(
            '{{%fk-employee_rel_subject-subject_id}}',
            '{{%employee_rel_subject}}'
        );

        // drops index for column `subject_id`
        $this->dropIndex(
            '{{%idx-employee_rel_subject-subject_id}}',
            '{{%employee_rel_subject}}'
        );

        // drops foreign key for table `{{%employees}}`
        $this->dropForeignKey(
            '{{%fk-employee_rel_subject-employee_id}}',
            '{{%employee_rel_subject}}'
        );

        // drops index for column `employee_id`
        $this->dropIndex(
            '{{%idx-employee_rel_subject-employee_id}}',
            '{{%employee_rel_subject}}'
        );

        $this->dropTable('{{%employee_rel_subject}}');
    }
}
