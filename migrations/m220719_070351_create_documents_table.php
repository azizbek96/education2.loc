<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%documents}}`.
 */
class m220719_070351_create_documents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%documents}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(11),
            'employee_id' => $this->integer(11),
            'reg_date' => $this->datetime(),
            'status' => $this->integer(1)->defaultValue(1),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'updated_at' => $this->integer(11)
        ]);

        // creates index for column `course_id`
        $this->createIndex(
            '{{%idx-documents-course_id}}',
            '{{%documents}}',
            'course_id'
        );

        // add foreign key for table `{{%courses}}`
        $this->addForeignKey(
            '{{%fk-documents-course_id}}',
            '{{%documents}}',
            'course_id',
            '{{%courses}}',
            'id',
            'CASCADE'
        );

        // creates index for column `employee_id`
        $this->createIndex(
            '{{%idx-documents-employee_id}}',
            '{{%documents}}',
            'employee_id'
        );

        // add foreign key for table `{{%employees}}`
        $this->addForeignKey(
            '{{%fk-documents-employee_id}}',
            '{{%documents}}',
            'employee_id',
            '{{%employees}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%courses}}`
        $this->dropForeignKey(
            '{{%fk-documents-course_id}}',
            '{{%documents}}'
        );

        // drops index for column `course_id`
        $this->dropIndex(
            '{{%idx-documents-course_id}}',
            '{{%documents}}'
        );

        // drops foreign key for table `{{%employees}}`
        $this->dropForeignKey(
            '{{%fk-documents-employee_id}}',
            '{{%documents}}'
        );

        // drops index for column `employee_id`
        $this->dropIndex(
            '{{%idx-documents-employee_id}}',
            '{{%documents}}'
        );

        $this->dropTable('{{%documents}}');
    }
}
