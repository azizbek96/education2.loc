<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%student_waiting}}`.
 */
class m220719_072339_create_student_waiting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%student_waiting}}', [
            'id' => $this->primaryKey(),
            'student_id'=>$this->integer(11),
            'subject_level_id'=>$this->integer(11),
            'wait_date'=>$this->date(),
            'status'=>$this->smallInteger()->defaultValue(1),
            'created_by'=>$this->integer(11),
            'updated_by'=>$this->integer(11),
            'created_at'=>$this->integer(11),
            'updated_at'=>$this->integer(11),
        ]);

        $this->createIndex(
            '{{%idx-student_waiting-student_id}}',
            '{{%student_waiting}}',
            'student_id'
        );

        $this->addForeignKey(
            '{{%fk-student_waiting-student_id}}',
            '{{%student_waiting}}',
            'student_id',
            '{{students}}',
            'id'
        );

        $this->createIndex(
            '{{%idx-student_waiting-subject_level_id}}',
            '{{%student_waiting}}',
            'subject_level_id'
        );

        $this->addForeignKey(
            '{{%fk-student_waiting-subject_level_id}}',
            '{{%student_waiting}}',
            'subject_level_id',
            '{{subject_level}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            '{{%idx-student_waiting-student_id}}',
            '{{%student_waiting}}'
        );

        $this->dropForeignKey(
            '{{%fk-student_waiting-student_id}}',
            '{{%student_waiting}}'
        );
        $this->dropIndex(
            '{{%idx-student_waiting-subject_level_id}}',
            '{{%student_waiting}}'
        );

        $this->dropForeignKey(
            '{{%fk-student_waiting-subject_level_id}}',
            '{{%student_waiting}}'
        );
        $this->dropTable('{{%student_waiting}}');
    }
}
