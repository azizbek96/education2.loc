<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%attendance}}`.
 */
class m220719_064253_create_attendance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%attendance}}', [
            'id' => $this->primaryKey(),
            'employee_id'=>$this->integer(11),
            'course_id'=>$this->integer(11),
            'attendance_date'=>$this->date(),
            'status'=>$this->smallInteger()->defaultValue(1),
            'created_by'=>$this->integer(11),
            'updated_by'=>$this->integer(11),
            'created_at'=>$this->integer(11),
            'updated_at'=>$this->integer(11),
        ]);
        $this->createIndex(
            '{{%idx-attendance-employee_id}}',
            '{{%attendance}}',
            'employee_id'
        );
        $this->addForeignKey(
            '{{%fk-attendance-employee_id}}',
            '{{%attendance}}',
            'employee_id',
            '{{%employees}}',
            'id',
            'RESTRICT'
        );
        $this->createIndex(
            '{{%idx-attendace-course_id}}',
            '{{%attendance}}',
            'course_id'
        );

        $this->addForeignKey(
            '{{%fk-attendace-course_id}}',
            '{{%attendance}}',
            'course_id',
            '{{%courses}}',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            '{{%idx-attendance-employee_id}}',
            '{{%attendance}}'
        );
        $this->dropForeignKey(
            '{{%fk-attendance-employee_id}}',
            '{{%attendance}}'
        );

        $this->dropIndex(
            '{{%idx-attendance-course_id}}',
            '{{%attendance}}'
        );
        $this->dropForeignKey(
            '{{%fk-attendance-course_id}}',
            '{{%attendance}}'
        );

        $this->dropTable('{{%attendance}}');
    }
}
