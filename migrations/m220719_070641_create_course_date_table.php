<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%course_date}}`.
 */
class m220719_070641_create_course_date_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%course_date}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'type' => $this->smallInteger()->defaultValue(1),
            'course_id' => $this->integer()->notNull(),
            'course_time' => $this->time(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        // creates index for column `course_id`
        $this->createIndex(
            '{{%idx-course_date-course_id}}',
            '{{%course_date}}',
            'course_id'
        );

        // add foreign key for table `{{%courses}}`
        $this->addForeignKey(
            '{{%fk-course_date-course_id}}',
            '{{%course_date}}',
            'course_id',
            '{{%courses}}',
            'id',
            'RESTRICT'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%courses}}`
        $this->dropForeignKey(
            '{{%fk-course_date-course_id}}',
            '{{%course_date}}'
        );

        // drops index for column `course_id`
        $this->dropIndex(
            '{{%idx-course_date-course_id}}',
            '{{%course_date}}'
        );

        $this->dropTable('{{%course_date}}');
    }
}
