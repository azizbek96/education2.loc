<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%courses}}`.
 */
class m220719_062545_create_courses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%courses}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'subject_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'subject_level_id' => $this->integer()->notNull(),
            'start_date' => $this->date(),
            'end_date' => $this->date(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `subject_id`
        $this->createIndex(
            '{{%idx-courses-subject_id}}',
            '{{%courses}}',
            'subject_id'
        );

        // add foreign key for table `{{%subject}}`
        $this->addForeignKey(
            '{{%fk-courses-subject_id}}',
            '{{%courses}}',
            'subject_id',
            '{{%subject}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `employee_id`
        $this->createIndex(
            '{{%idx-courses-employee_id}}',
            '{{%courses}}',
            'employee_id'
        );

        // add foreign key for table `{{%employees}}`
        $this->addForeignKey(
            '{{%fk-courses-employee_id}}',
            '{{%courses}}',
            'employee_id',
            '{{%employees}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `subject_level_id`
        $this->createIndex(
            '{{%idx-courses-subject_level_id}}',
            '{{%courses}}',
            'subject_level_id'
        );

        // add foreign key for table `{{%subject_level}}`
        $this->addForeignKey(
            '{{%fk-courses-subject_level_id}}',
            '{{%courses}}',
            'subject_level_id',
            '{{%subject_level}}',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%subject}}`
        $this->dropForeignKey(
            '{{%fk-courses-subject_id}}',
            '{{%courses}}'
        );

        // drops index for column `subject_id`
        $this->dropIndex(
            '{{%idx-courses-subject_id}}',
            '{{%courses}}'
        );

        // drops foreign key for table `{{%employees}}`
        $this->dropForeignKey(
            '{{%fk-courses-employee_id}}',
            '{{%courses}}'
        );

        // drops index for column `employee_id`
        $this->dropIndex(
            '{{%idx-courses-employee_id}}',
            '{{%courses}}'
        );

        // drops foreign key for table `{{%subject_level}}`
        $this->dropForeignKey(
            '{{%fk-courses-subject_level_id}}',
            '{{%courses}}'
        );

        // drops index for column `subject_level_id`
        $this->dropIndex(
            '{{%idx-courses-subject_level_id}}',
            '{{%courses}}'
        );

        $this->dropTable('{{%courses}}');
    }
}
