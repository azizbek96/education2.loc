<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employees}}`.
 */
class m220719_061411_create_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employees}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(50),
            'last_name' => $this->string(50),
            'age' => $this->integer(11),
            'phone' => $this->string(20),
            'photo' => $this->string(120),
            'email' => $this->string(50),
            'file' => $this->string(120),
            'position_id' => $this->integer(11),
            'status' => $this->integer(1)->defaultValue(1),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        // creates index for column `position_id`
        $this->createIndex(
            '{{%idx-employees-position_id}}',
            '{{%employees}}',
            'position_id'
        );

        // add foreign key for table `{{%position}}`
        $this->addForeignKey(
            '{{%fk-employees-position_id}}',
            '{{%employees}}',
            'position_id',
            '{{%position}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-employees-created_by}}',
            '{{%employees}}',
            'created_by'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-employees-created_by}}',
            '{{%employees}}',
            'created_by',
            '{{%users}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-employees-updated_by}}',
            '{{%employees}}',
            'updated_by'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-employees-updated_by}}',
            '{{%employees}}',
            'updated_by',
            '{{%users}}',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%position}}`
        $this->dropForeignKey(
            '{{%fk-employees-position_id}}',
            '{{%employees}}'
        );

        // drops index for column `position_id`
        $this->dropIndex(
            '{{%idx-employees-position_id}}',
            '{{%employees}}'
        );

        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-employees-created_by}}',
            '{{%employees}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-employees-created_by}}',
            '{{%employees}}'
        );

        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-employees-updated_by}}',
            '{{%employees}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-employees-updated_by}}',
            '{{%employees}}'
        );

        $this->dropTable('{{%employees}}');
    }
}
