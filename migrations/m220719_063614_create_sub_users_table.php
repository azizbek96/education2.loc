<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sub_users}}`.
 */
class m220719_063614_create_sub_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sub_users}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11),
            'image' => $this->string(255),
        ]);
        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-sub_users-user_id}}',
            '{{%sub_users}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-sub_users-user_id}}',
            '{{%sub_users}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-sub_users-user_id}}',
            '{{%sub_users}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-sub_users-user_id}}',
            '{{%sub_users}}'
        );

        $this->dropTable('{{%sub_users}}');
    }
}
