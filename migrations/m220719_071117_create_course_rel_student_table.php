<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%course_rel_student}}`.
 */
class m220719_071117_create_course_rel_student_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%course_rel_student}}', [
            'id' => $this->primaryKey(),
            'course_id'=>$this->integer(11),
            'student_id'=>$this->integer(11),
            'start_date'=>$this->date(),
            'end_date'=>$this->date(),
            'created_by'=>$this->integer(11),
            'updated_by'=>$this->integer(11),
            'created_at'=>$this->integer(11),
            'updated_at'=>$this->integer(11),
        ]);

        $this->createIndex(
            '{{%idx-course_rel_student-course_id}}',
            '{{%course_rel_student}}',
            'course_id'
        );

        $this->addForeignKey(
            '{{%fk-course_rel_student-course_id}}',
            '{{%course_rel_student}}',
            'course_id',
            '{{%courses}}',
            'id'
        );

        $this->createIndex(
            '{{%idx-course_rel_student-student_id}}',
            '{{%course_rel_student}}',
            'student_id'
        );

        $this->addForeignKey(
            '{{%fak-course_rel_student-student_id}}',
            '{{%course_rel_student}}',
            'student_id',
            '{{%students}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            '{{%fk-course_rel_student-course_id}}',
            '{{%course_rel_student}}'
        );

        $this->dropIndex(
            '{{%idx-course_rel_student-course_id}}',
            '{{%course_rel_student}}'
        );
        $this->dropForeignKey(
            '{{%fak-course_rel_student-student_id}}',
            '{{%course_rel_student}}'
        );

        $this->dropIndex(
            '{{%idx-course_rel_student-student_id}}',
            '{{%course_rel_student}}'
        );
        $this->dropTable('{{%course_rel_student}}');
    }
}
