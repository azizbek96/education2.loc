<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%document_items}}`.
 */
class m220719_072118_create_document_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%document_items}}', [
            'id' => $this->primaryKey(),
            'student_id' => $this->integer(11),
            'price' => $this->decimal(20,3),
            'price_date' => $this->date(),
            'type' => $this->integer(),
            'status' => $this->integer(1)->defaultValue(1),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        // creates index for column `student_id`
        $this->createIndex(
            '{{%idx-document_items-student_id}}',
            '{{%document_items}}',
            'student_id'
        );

        // add foreign key for table `{{%students}}`
        $this->addForeignKey(
            '{{%fk-document_items-student_id}}',
            '{{%document_items}}',
            'student_id',
            '{{%students}}',
            'id',
            'RESTRICT'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%students}}`
        $this->dropForeignKey(
            '{{%fk-document_items-student_id}}',
            '{{%document_items}}'
        );

        // drops index for column `student_id`
        $this->dropIndex(
            '{{%idx-document_items-student_id}}',
            '{{%document_items}}'
        );

        $this->dropTable('{{%document_items}}');
    }
}
