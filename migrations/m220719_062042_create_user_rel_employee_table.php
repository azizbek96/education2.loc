<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_rel_employee}}`.
 */
class m220719_062042_create_user_rel_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_rel_employee}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-user_rel_employee-user_id}}',
            '{{%user_rel_employee}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-user_rel_employee-user_id}}',
            '{{%user_rel_employee}}',
            'user_id',
            '{{%users}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `employee_id`
        $this->createIndex(
            '{{%idx-user_rel_employee-employee_id}}',
            '{{%user_rel_employee}}',
            'employee_id'
        );

        // add foreign key for table `{{%employees}}`
        $this->addForeignKey(
            '{{%fk-user_rel_employee-employee_id}}',
            '{{%user_rel_employee}}',
            'employee_id',
            '{{%employees}}',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-user_rel_employee-user_id}}',
            '{{%user_rel_employee}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-user_rel_employee-user_id}}',
            '{{%user_rel_employee}}'
        );

        // drops foreign key for table `{{%employees}}`
        $this->dropForeignKey(
            '{{%fk-user_rel_employee-employee_id}}',
            '{{%user_rel_employee}}'
        );

        // drops index for column `employee_id`
        $this->dropIndex(
            '{{%idx-user_rel_employee-employee_id}}',
            '{{%user_rel_employee}}'
        );

        $this->dropTable('{{%user_rel_employee}}');
    }
}
